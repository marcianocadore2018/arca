/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  marciano.cadore
 * Created: 01/04/2020
 */

create table usuario(
id serial primary key,
login varchar(30) not null unique, 
tipousuario varchar(1) not null,
nomeusuario varchar(30) not null unique,
senhausuario varchar(30) not null,
alteracaosenha int default 0 not null,
datacadastro date not null);

insert into usuario(login, senhausuario, tipousuario, nomeusuario) values('adminarca','adminarca123456&','M','Admin Arca');

create table estado(
id serial primary key,
nomeestado varchar(80) unique not null,
uf varchar(2) unique not null);

insert into estado (nomeestado, uf) values('Acre', 'AC');
insert into estado (nomeestado, uf) values('Amapá','AP');
insert into estado (nomeestado, uf) values('Amazonas','AM');
insert into estado (nomeestado, uf) values('Bahia','BA');
insert into estado (nomeestado, uf) values('Ceará','CE');
insert into estado (nomeestado, uf) values('Distrito Federal','DF');
insert into estado (nomeestado, uf) values('Espirito Santo','ES');
insert into estado (nomeestado, uf) values('Goiás','GO');
insert into estado (nomeestado, uf) values('Maranhão','MA');
insert into estado (nomeestado, uf) values('Mato Grosso','MT');
insert into estado (nomeestado, uf) values('Mato Grosso do Sul','MS');
insert into estado (nomeestado, uf) values('Minas Gerais','MG');
insert into estado (nomeestado, uf) values('Pará','PA');
insert into estado (nomeestado, uf) values('Paraíba','PB');
insert into estado (nomeestado, uf) values('Paraná','PR');
insert into estado (nomeestado, uf) values('Pernambuco','PE');
insert into estado (nomeestado, uf) values('Piauí','PI');
insert into estado (nomeestado, uf) values('Rio de Janeiro','RJ');
insert into estado (nomeestado, uf) values('Rio Grando do Norte','RN');
insert into estado (nomeestado, uf) values('Rio Grande do Sul','RS');
insert into estado (nomeestado, uf) values('Rondônia','RO');
insert into estado (nomeestado, uf) values('Roraima','RR');
insert into estado (nomeestado, uf) values('Santa Catarina','SC');
insert into estado (nomeestado, uf) values('São Paulo','SP');
insert into estado (nomeestado, uf) values('Sergipe','SE');
insert into estado (nomeestado, uf) values('Tocantis','TO');

create table cidade(
id serial primary key,
nome varchar(300) unique not null,
idestado int not null,
foreign key (idestado) references estado(id));

insert into cidade(nome, idestado) values('erechim', 21);
insert into cidade(nome, idestado) values('itatiba do sul', 21);
insert into cidade(nome, idestado) values('barra do do azul', 21);
insert into cidade(nome, idestado) values('passo fundo', 21);

create table categoria(
id serial primary key,
descricao varchar(200) not null,
tipopessoa varchar(2) not null);

create table embalagens(
id serial primary key,
descricao varchar(200) not null unique);

insert into embalagens(descricao) values('Sacaria');
insert into embalagens(descricao) values('BigBag');

create table frete(
id serial primary key,
origem varchar(300) not null,
destino varchar(300) not null,
valor numeric not null,
datafrete varchar(20) not null);

create table cliente(
id serial primary key,
nome varchar(200),
tipo_pessoa char(2) not null,
datanascimento date,
genero char(1),
rg varchar(10),
cpf varchar(14),
observacao text,
endereco varchar(200) not null,
bairro varchar(200) not null,
numero varchar(10) not null,
idcidade int not null,
idestado int not null,
cep varchar(9) not null,
complemento varchar(200),
telefone varchar(20),
celular varchar(20),
contato varchar(200),
email varchar(300),
skype varchar(200),
cnpj varchar(30),
razao_social varchar(200),
inscricao_estadual varchar(30),
nome_representante varchar(300), 
nome_arquivo varchar(300),
caminho_arquivo varchar(300),
id_categoria int not null,
nome_foto varchar(300),
caminho_foto varchar(300),
foreign key(id_categoria) references categoria(id),
foreign key(id_cidade) references cidade(id));

create table fornecedor(
id serial primary key,
idcategoria int not null,
nome varchar(200),
tipo_pessoa char(2) not null,
datanascimento date,
genero char(1),
rg varchar(10),
cpf varchar(14),
observacao text,
endereco varchar(200) not null,
bairro varchar(200) not null,
numero varchar(10) not null,
idcidade int not null,
idestado int not null,
cep varchar(9) not null,
complemento varchar(200),
telefone varchar(20),
celular varchar(20),
contato varchar(200),
email varchar(300),
skype varchar(200),
cnpj varchar(30),
razao_social varchar(200),
inscricao_estadual varchar(30),
nome_representante varchar(300), 
nome_arquivo varchar(300),
caminho_arquivo varchar(300),
nome_foto varchar(300),
caminho_foto varchar(300),
foreign key(idcategoria) references categoria(id),
foreign key(idcidade) references cidade(id));

create table vendedor(
id serial primary key,
idcategoria int not null,
nome varchar(200),
tipo_pessoa char(2) not null,
datanascimento date,
genero char(1),
rg varchar(10),
cpf varchar(14),
conta_deposito varchar(20),
agencia_deposito varchar(20),
observacao text,
endereco varchar(200) not null,
bairro varchar(200) not null,
numero varchar(10) not null,
idcidade int not null,
idestado int not null,
cep varchar(9) not null,
complemento varchar(200),
telefone varchar(20),
celular varchar(20),
contato varchar(200),
email varchar(300),
skype varchar(200),
cnpj varchar(30),
razao_social varchar(200),
inscricao_estadual varchar(30),
nome_representante varchar(300), 
nome_arquivo varchar(300),
caminho_arquivo varchar(300),
nome_foto varchar(300),
caminho_foto varchar(300),
foreign key(idcategoria) references categoria(id),
foreign key(idcidade) references cidade(id));

create table transportadora(
id serial primary key,
idcategoria int not null,
nome varchar(200),
tipo_pessoa char(2) not null,
datanascimento date,
genero char(1),
rg varchar(10),
cpf varchar(14),
observacao text,
endereco varchar(200) not null,
bairro varchar(200) not null,
numero varchar(10) not null,
idcidade int not null,
idestado int not null,
cep varchar(9) not null,
complemento varchar(200),
telefone varchar(20),
celular varchar(20),
contato varchar(200),
email varchar(300),
skype varchar(200),
cnpj varchar(30),
razao_social varchar(200),
inscricao_estadual varchar(30),
nome_representante varchar(300), 
nome_arquivo varchar(300),
caminho_arquivo varchar(300),
nome_foto varchar(300),
caminho_foto varchar(300),
foreign key(idcategoria) references categoria(id),
foreign key(idcidade) references cidade(id));

create table tsi(
id serial primary key,
nome varchar(100) not null unique,
descricao text not null,
preco numeric not null);

create table frete(
id serial primary key,
origem varchar(200) not null,
destino varchar(200) not null,
valor numeric not null);
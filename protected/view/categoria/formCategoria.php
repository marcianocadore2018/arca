<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Categoria</div>
        <div class="panel-body">
            <form action="<?= $acao ?>" name="formCategoria" id="formCategoria" method="POST" class="form" role="form">
                <input type="hidden" class="form-control" id="id" name="id" readonly="true" value="<?php if (isset($categoria)) echo $categoria['id']; ?>">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($categoria)) echo $categoria['id']; ?>">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <label for="descricao">Descrição</label>
                        <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Digite a Descrição" 
                               value="<?php if (isset($categoria)) echo $categoria['descricao']; ?>" required minlength="3" maxlength="100">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <label for="tipopessoa">Tipo Pessoa</label>
                        <br>
                        <input type="radio" name="tipopessoa" value="CL" checked=""  
                        <?php
                        if (isset($categoria))
                            if ($categoria['tipopessoa'] == 'CL') {
                                echo 'checked';
                            } else {
                                $categoria == null;
                            }
                        ?>> Cliente
                        
                        <input type="radio" name="tipopessoa" value="FO"
                        <?php
                        if (isset($categoria))
                            if ($categoria['tipopessoa'] == 'FO') {
                                echo 'checked';
                            } else {
                                $categoria == null;
                            }
                        ?>> Fornecedor
                        <input type="radio" name="tipopessoa" value="VE" 
                        <?php
                        if (isset($categoria))
                            if ($categoria['tipopessoa'] == 'VE') {
                                echo 'checked';
                            } else {
                                $categoria == null;
                            }
                        ?>> Vendedor
                        <input type="radio" name="tipopessoa" value="TP" 
                        <?php
                        if (isset($categoria))
                            if ($categoria['tipopessoa'] == 'TP') {
                                echo 'checked';
                            } else {
                                $categoria == null;
                            }
                        ?>> Transportadora
                    </div>
                    <br/>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>

<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
$("#formCategoria").validate({
    rules: {
        descricao: {
            required: true
        }
    },
    messages: {
        descricao: {
            required: "Por favor, informe a Descrição",
            minlength: "A Descrição deve ter pelo menos 3 caracteres",
            maxlength: "A Descrição deve ter no máximo 200 caracteres"
        }
    }
});
</script>
<div id="fundo">
    <div class="">
        <div class="panel panel-primary">
            <div class="panel-heading">Relação de Categorias</div>
            <div class="panel-body">
                    <a href="index.php?controle=categoriaController&acao=novo">
                        <span class='glyphicon glyphicon-plus'> Adicionar</span>
                    </a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>ID</th>
                    <th>Descrição</th>
                    <th>Tipo Pessoa</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($listaDados as $item) {
                                echo '<tr>';
                                $id = $item['id'];
                                echo '<td style="width: 40px; text-align: center">' . $item['id'];
                                echo '<td>' . $item['descricao'];
                                if ($item['tipopessoa'] == 'CL'){
                                  $categoria = "Cliente";  
                                }else if($item['tipopessoa'] == 'FO'){
                                  $categoria = "Fornecedor";  
                                }else if($item['tipopessoa'] == 'TP'){
                                  $categoria = "Transportadora";    
                                }else if($item['tipopessoa'] == 'VE'){
                                  $categoria = "Vendedor";  
                                }
                                echo '<td>' . $categoria;

                                $string = 'passar' . $id . 'metodoget';
                                $idencriptografa = base64_encode($string);
                                echo "<td> <a href='index.php?controle=categoriaController&acao=buscar&id=$idencriptografa'>"
                                . " <span class='glyphicon glyphicon-pencil'> </span>"
                                . "</a> </td>";
                                echo "<td> <a onclick='excluir(\"excluir\",\"categoriaController\",\"$idencriptografa\")' href='#'>"
                                . " <span class='glyphicon glyphicon-trash customDialog'> </span>"
                                . "</a> </td>";

                                echo '</tr>';
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Embalagem</div>
        <div class="panel-body">
            <form action="<?= $acao ?>" name="formEmbalagem" id="formEmbalagem" method="POST" class="form" role="form">
                <input type="hidden" class="form-control" id="id" name="id" readonly="true" value="<?php if (isset($embalagem)) echo $tsi['id']; ?>">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($embalagem)) echo $embalagem['id']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="descricao">Descrição</label>
                        <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Digite a Descrição da Embalagem" 
                               value="<?php if (isset($embalagem)) echo $embalagem['descricao']; ?>" required minlength="3" maxlength="100">
                    </div>
                </div>
                
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>

<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
$("#formEmbalagem").validate({
    rules: {
        descricao: {
            required: true
        }
    },
    messages: {
        descricao: {
            required: "Por favor, informe a Descrição"
        }
    }
});
</script>
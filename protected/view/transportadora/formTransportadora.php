<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Transportadora</div>
        <div class="panel-body">
            <form action="<?php echo $acao; ?>" name="formTransportadora" id="formTransportadora" method="POST" class="form"  enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($transportadora)) echo $transportadora['id']; ?>">
                    </div>
                </div>

                <script>
                    $(document).ready(function () {

                        $("#juspf").hide();
                        $("#juspj").hide();
                        $("#tipopessoa").change(function () {
                            loadSituation();
                        });


                        loadSituation();
                    });

                    function loadSituation() {
                        var val = $("#tipopessoa").val();
                        if (val == 'PF') {
                            $("#juspf").fadeIn();
                            $("#juspj").fadeOut();
                            $("#nome").attr("required", true);
                        } else {
                            $("#juspf").fadeOut();
                            $("#juspj").fadeIn();
                            $("#nome").attr("required", false);
                        }
                    }
                    ;
                </script>

                <div class="row">
                    <div class="col-md-2">
                        <label for="tipopessoa">Tipo Pessoa</label>
                        <select class="form-control" name="tipopessoa" id="tipopessoa" required data-errormessage-value-missing="Selecione o Tipo de Pessoa">


                            <?php if (($_GET['acao'] == 'buscar') && ($transportadora['tipo_pessoa'] == 'PF')) { ?>
                                <option value="PF" selected="">Pessoa Física</option>
                            <?php } else if (($_GET['acao'] == 'buscar') && ($transportadora['tipo_pessoa'] == 'PJ')) { ?>
                                <option value="PJ" selected="">Pessoa Jurídica</option>
                            <?php } ?>  
                            ?>
                            <!-- Novo registro-->
                            <?php if ($_GET['acao'] == 'novo') { ?>
                                <option value="PJ">Pessoa Jurídica</option>
                            <?php }
                            ?>
                        </select>
                        <br>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <label for="idcategoria">Categoria</label>
                        <select class="form-control" name="idcategoria" id="idcategoria" required data-errormessage-value-missing="Selecione a Categoria">
                            <option value=''>Selecione a Categoria</option>
                            <?php
                            foreach ($listaCategorias as $categorias) {
                                $selected = (isset($transportadora) && $transportadora['idcategoria'] == $categorias['id']) ? 'selected' : '';
                                ?>
                                <option value='<?php echo $categorias['id']; ?>'
                                        <?php echo $selected; ?>> 
                                            <?php echo $categorias['descricao']; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="row" id="juspf">
                    <div class="col-md-8">
                        <label for="nome">Nome</label>
                        <input type="text" class="form-control" id="nome" name="nome" placeholder="Digite o Nome" 
                               value="<?php if (isset($transportadora)) echo $transportadora['nome']; ?>" required minlength="3" maxlength="100">
                    </div>
                    <div class="col-md-3">
                        <label for="datanascimento">Data/Nascimento</label>
                        <input type="text" class="form-control" id="data" name="datanascimento" placeholder="Informe a Data/Nascimento" 
                               value="<?php if (isset($transportadora)) echo $transportadora['datanascimento']; ?>" required>
                    </div>
                    <div class="col-md-3">
                        <label for="sexo">Gênero</label>
                        <br>
                        <input type="radio" name="sexo" value="M" checked="" 
                        <?php
                        if (isset($transportadora))
                            if ($transportadora['sexo'] == 'M') {
                                echo 'checked';
                            } else {
                                $transportadora == null;
                            }
                        ?>> Masculino
                        <input type="radio" name="sexo" value="F" 
                        <?php
                        if (isset($transportadora))
                            if ($transportadora['sexo'] == 'F') {
                                echo 'checked';
                            } else {
                                $transportadora == null;
                            }
                        ?>> Feminino
                    </div>
                    <div class="col-md-2">
                        <label for="rg">RG</label>
                        <input type="text" class="form-control" id="rg" name="rg" placeholder="Informe o RG" 
                               value="<?php if (isset($transportadora)) echo $transportadora['rg']; ?>" required maxlength="10">
                    </div>
                    <div class="col-md-2">
                        <label for="cpf">CPF</label>
                        <input type="text" class="form-control" id="cpf_atualizado" name="cpf" placeholder="Informe o CPF" 
                               onkeypress="return Onlynumbers(event)" value="<?php if (isset($transportadora)) echo $transportadora['cpf']; ?>" required maxlength="14">
                    </div>
                </div>

                <div class="row" id="juspj">
                    <div class="col-md-3">
                        <label for="razaosocial">Razão Social</label>
                        <input type="text" class="form-control" id="razao_social" name="razao_social" placeholder="Digite a Razão Social" 
                               value="<?php if (isset($transportadora)) echo $transportadora['razao_social']; ?>" required maxlength="200">
                    </div>
                    <div class="col-md-3">
                        <label for="cnpj">CNPJ</label>
                        <input type="text" class="form-control" id="cnpj" name="cnpj" placeholder="Digite o CNPJ" 
                               value="<?php if (isset($transportadora)) echo $transportadora['cnpj']; ?>" required maxlength="20">
                    </div>
                    <div class="col-md-2">
                        <label for="inscricao_estadual">Inscri&ccedil;&atilde;o Estadual</label>
                        <input type="text" class="form-control" name="inscricao_estadual" placeholder="Inscrição Estadual" 
                               value="<?php if (isset($transportadora)) echo $transportadora['inscricao_estadual']; ?>">
                    </div>
                    <div class="col-md-4" id="contatotransportadora">
                        <label for="nome_representante">Nome Representante</label>
                        <input type="text" class="form-control" id="nome_representante" name="nome_representante" placeholder="Informe o Nome do Representante" 
                               value="<?php if (isset($transportadora)) echo $transportadora['nome_representante']; ?>">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <label for="observacao">Observação</label>
                        <textarea class="form-control" id="observacao" name="observacao" maxlength="2000"><?php if (isset($transportadora)) echo $transportadora['observacao']; ?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="endereco">Endereço</label>
                        <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Digite o Endereço" 
                               value="<?php if (isset($transportadora)) echo $transportadora['endereco']; ?>" required minlength="3" maxlength="100">
                    </div>
                    <div class="col-md-3">
                        <label for="bairro">Bairro</label>
                        <input type="text" class="form-control" id="bairro" name="bairro" placeholder="Digite o Bairro" 
                               value="<?php if (isset($transportadora)) echo $transportadora['bairro']; ?>" required minlength="3" maxlength="80">
                    </div>
                    <div class="col-md-3">
                        <label for="numero">Número</label>
                        <input type="text" class="form-control" id="numero" name="numero" maxlength="5" placeholder="Digite a Número" 
                               value="<?php if (isset($transportadora)) echo $transportadora['numero']; ?>" onkeypress="return Onlynumbers(event);" required>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <label for="idcidade">Cidade</label>
                        <select class="form-control" name="idcidade" id="idcidade" required data-errormessage-value-missing="Selecione a Cidade">
                            <option value=''>Selecione a Cidade</option>
                            <?php
                            foreach ($listaCidades as $cidades) {
                                $selected = (isset($transportadora) && $transportadora['idcidade'] == $cidades['id']) ? 'selected' : '';
                                ?>
                                <option value='<?php echo $cidades['id']; ?>'
                                        <?php echo $selected; ?>> 
                                            <?php echo $cidades['nomecidade']; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label for="idestado">Estado</label>
                        <select class="form-control" name="idestado" id="idestado" required data-errormessage-value-missing="Selecione o Estado">
                            <?php
                            foreach ($listaEstados as $estados) {
                                $selected = (isset($transportadora) && $transportadora['idestado'] == $estados['id']) ? 'selected' : '';
                                ?>
                                <option value='<?php echo $estados['id']; ?>'
                                        <?php echo $selected; ?>> 
                                            <?php echo $estados['uf']; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label for="cep">CEP</label>
                        <input type="text" class="form-control" id="cep" name="cep" placeholder="Digite o Cep" 
                               value="<?php if (isset($transportadora)) echo $transportadora['cep']; ?>" required maxlength="9">
                    </div>
                    <!--<div style="padding-top: 30px; font-size: 12px; color: blue">
                        <a href="http://m.correios.com.br/movel/buscaCep.do" target="_blank" title="Clique aqui para pesquisar o CEP"><b>Verificar CEP Correios</b></a>
                    </div>-->
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <label for="complemento">Complemento</label>
                        <input type="text" class="form-control" id="complemento" name="complemento" placeholder="Digite a Complemento" 
                               value="<?php if (isset($transportadora)) echo $transportadora['complemento']; ?>">
                    </div>
                </div>
                <hr/>

                <div class="row">
                    <div class="col-md-3">
                        <label for="telefone">Telefone</label>
                        <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Digite o Telefone" 
                               value="<?php if (isset($transportadora)) echo $transportadora['telefone']; ?>" required maxlength="20">
                    </div>
                    <div class="col-md-2">
                        <label for="celular">Celular</label>
                        <input type="text" class="form-control" attrname="telephone1" name="celular" placeholder="Digite o Celular" 
                               value="<?php if (isset($transportadora)) echo $transportadora['celular']; ?>">
                    </div>
                    <div class="col-md-4" id="contatotransportadora">
                        <label for="contato">Contato</label>
                        <input type="text" class="form-control" id="contato" name="contato" placeholder="Informe o Contato Próximo" 
                               value="<?php if (isset($transportadora)) echo $transportadora['contato']; ?>">
                    </div>
                    <div class="col-md-4">
                        <label for="email">E-mail</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Informe o E-mail" 
                               value="<?php if (isset($transportadora)) echo $transportadora['email']; ?>">
                    </div>
                    <div class="col-md-4">
                        <label for="skype">Skype</label>
                        <input type="text" class="form-control" id="skype" name="skype" placeholder="Informe o Skype" 
                               value="<?php if (isset($transportadora)) echo $transportadora['skype']; ?>">
                    </div>
                </div>

                <div class="col-md-12">
                    <label for="arquivo">Arquivo</label>
                    <input name="arquivo" type="file"/>
                    <?php
                    //Cair� neste teste sempre que o usu�rio for alterar um registro
                    if (isset($_SERVER['HTTPS'])) {
                        $prefixo = 'https://';
                    } else {
                        $prefixo = 'http://';
                    }
                    if (isset($transportadora['arquivo']) != null) {
                        if ($transportadora['arquivo'] == '') {
                            $urlbase = $prefixo . '' . $_SERVER['HTTP_HOST'] . '/arca/protected/imagens/noimagens/noimagem.png';
                            if ($transportadora['arquivo'] == "1"){
                              echo '<img src = "' . "$urlbase" . '" style="width: 100px;"/>';   
                            }
                        } else {
                            $urlbase = $prefixo . '' . $_SERVER['HTTP_HOST'] . '/'. $transportadora['arquivo'];
                            if($transportadora['nome_arquivo'] != ""){
                              echo '<a href="'.$urlbase.'" download title="Clique para fazer download">Baixar arquivo</a>';
                            }
                        }
                    }//Cair� nesse teste sempre que o usu�rio for inserir um novo arquivo
                    else {
                        if (isset($_SERVER['HTTPS'])) {
                            $prefixo = 'https://';
                        } else {
                            $prefixo = 'http://';
                        }
                        $urlbase = $prefixo . '' . $_SERVER['HTTP_HOST'] . '/arca/protected/imagens/noimagens/noimagem.png';
                        if($_GET['acao'] != 'novo'){
                           echo '<img src = "' . "$urlbase" . '" style="width: 100px;"/>';
                        }
                    }
                    ?>
                    <br/>
                </div>
                <div class="col-md-12">
                    <label for="foto">Foto Logo</label>
                    <input name="foto" type="file"/>
                    <?php
                    //Cair� neste teste sempre que o usu�rio for alterar um registro
                    if (isset($_SERVER['HTTPS'])) {
                        $prefixo = 'https://';
                    } else {
                        $prefixo = 'http://';
                    }
                    if (isset($transportadora['foto']) != null) {
                        if ($transportadora['foto'] == '') {
                            $urlbase = $prefixo . '' . $_SERVER['HTTP_HOST'] . '/arca/protected/imagens/noimagens/noimagem.png';
                            if ($transportadora['foto'] == "1"){
                              echo '<img src = "' . "$urlbase" . '" style="width: 100px;"/>';   
                            }
                        } else {
                            $urlbase = $prefixo . '' . $_SERVER['HTTP_HOST'] . '/'. $transportadora['foto'];
                            if($transportadora['nome_foto'] != "1"){
                              echo '<img src = "' . "$urlbase" . '" style="width: 100px;"/>'; 
                            }
                        }
                    }
                    else {
                        if (isset($_SERVER['HTTPS'])) {
                            $prefixo = 'https://';
                        } else {
                            $prefixo = 'http://';
                        }
                        $urlbase = $prefixo . '' . $_SERVER['HTTP_HOST'] . '/arca/protected/arquivos/noimagens/noimagem.png';
                        if($_GET['acao'] != 'novo'){
                           echo '<img src = "' . "$urlbase" . '" style="width: 100px;"/>';
                        }
                    }
                    ?>
                    <br/>
                </div>
                <div class="row col-md-12">
                    <button type="submit" class="btn btn-success">Gravar</button>
                    <button type="reset" class="btn btn-primary">Limpar</button>
                </div>    
            </form>
        </div>
    </div>
</div>

<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
    jQuery.extend(jQuery.validator.messages, {
        email: "Por favor informe um email válido."
    });

    $("#formTransportadora").validate({
        rules: {
            nome: {
                required: true
            },
            datanascimento: {
                required: true
            },
            rg: {
                required: true
            },
            cpf: {
                required: true
            },
            justificativatransportadorainativo: {
                required: true
            },
            endereco: {
                required: true
            },
            bairro: {
                required: true
            },
            numero: {
                required: true
            },
            idcidade: {
                required: true
            },
            idestado: {
                required: true
            },
            idcategoria: {
                required: true
            },
            cep: {
                required: true
            },
            telefone: {
                required: true
            },
            cnpj: {
                required: true
            },
            inscricao_estadual: {
                required: true
            },
            razao_social: {
                required: true
            }
        },
        messages: {
            nome: {
                required: "Por favor, informe o Nome do Transportadora",
                minlength: "O Nome deve ter pelo menos 3 caracteres",
                maxlength: "O Nome deve ter no máximo 100 caracteres"
            },
            datanascimento: {
                required: "Por favor, informe a Data de Nascimento"
            },
            rg: {
                required: "Por favor, informe o RG"
            },
            cpf: {
                required: "Por favor, informe o CPF"
            },
            justificativatransportadorainativo: {
                required: "Por favor, informe a Justificativa do Transportadora Inativo",
                minlength: "A Justificativa deve ter pelo menos 10 caracteres",
                maxlength: "A Justificativa deve ter no máximo 300 caracteres"
            },
            endereco: {
                required: "Por favor, informe o Endereço",
                minlength: "O Endereço deve ter pelo menos 3 caracteres",
                maxlength: "O Endereço deve ter no máximo 100 caracteres"
            },
            bairro: {
                required: "Por favor, informe o Bairro",
                minlength: "O Bairro deve ter pelo menos 3 caracteres",
                maxlength: "O Bairro deve ter no máximo 100 caracteres"
            },
            numero: {
                required: "Por favor, Informe o Número"
            },
            idcidade: {
                required: "Por favor, Informe a Cidade"
            },
            idestado: {
                required: "Por favor, Informe o Estado"
            },
            idcategoria: {
                required: "Por favor, Selecione a Categoria"
            },
            cep: {
                required: "Por favor, informe o CEP",
                minlength: "O CEP deve ter pelo menos 9 caracteres",
                maxlength: "O CEP deve ter no máximo 9 caracteres"
            },
            telefone: {
                required: "Por favor, informe o Telefone",
                maxlength: "O Telefone deve ter no máximo 20 caracteres"
            },
            descricaorenda: {
                required: "Por favor, informe a Descrição da Renda",
                minlength: "A Descrição da Renda deve ter pelo menos 10 caracteres",
                maxlength: "A Descrição da Renda deve ter no máximo 100 caracteres"
            },
            valorrenda: {
                required: "Por favor, informe o Valor da Renda do Transportadora"
            },
            datainiciorenda: {
                required: "Por favor, informe a Data de Início da Renda"
            },
            cnpj: {
                required: "Por favor, informe o CNPJ"
            },

            inscricao_estadual: {
                required: "Por favor, informe a Inscri&ccedil;&atilde;o Estadual"
            },

            razao_social: {
                required: "Por favor, informe a Razão Social"
            }
        }
    });
</script>

<style>td.alinhamento_td{padding-left: 10px; text-align: left; vertical-align:middle !important;}</style>
<div id="fundo">
    <div class="">
        <div class="panel panel-primary">
            <div class="panel-heading">Relação de Clientes</div>
            <div class="panel-body">
                    <a href="index.php?controle=clienteController&acao=novo">
                        <span class='glyphicon glyphicon-plus'> Adicionar</span>
                    </a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1" style="width: 2000px;">
                    <thead>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>Logo</th>
                    <th>Nome / Razão Social</th>
                    <th>Categoria</th>
                    <th>RG / Inscrição Estadual</th>
                    <th>CPF / CNPJ</th>
                    <th>Endereço</th>
                    <th>Telefone</th>
                    <th>Celular</th>
                    <th>Documento</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($listaDados as $item) {
                            if($item['tipopessoa'] == 'PF'){
                              $cor_tipo_pessoa = 'background-color: #FFFAF0';  
                            }else{
                              $cor_tipo_pessoa = 'background-color: #FAF0E6';  
                            }
                            
                            echo '<tr style="'.$cor_tipo_pessoa.'">';
                            $id = $item['id'];
                            
                            $string = 'passar' . $id . 'metodoget';
                            $idencriptografa = base64_encode($string);
                            echo "<td class='alinhamento_td'> <a href='index.php?controle=clienteController&acao=buscar&id=$idencriptografa'>"
                            . " <span class='glyphicon glyphicon-pencil'> </span>"
                            . "</a> </td>";
                            echo "<td class='alinhamento_td'> <a onclick='excluir(\"excluir\",\"clienteController\",\"$idencriptografa\")' href='#'>"
                            . " <span class='glyphicon glyphicon-trash customDialog'> </span>"
                            . "</a> </td>";
                            
                            if ($item['nomecliente']){
                              $nome_cliente = $item['nomecliente'];  
                            }else{
                              $nome_cliente = $item['razao_social'];  
                            }
                            
                            if ($item['rg']){
                              $registro = "RG: " . $item['rg'];  
                            }else{
                              $registro = "IE: " . $item['inscricaoestadual'];  
                            }
                            
                            if ($item['cpf']){
                              $numero_identificacao = $item['cpf'];  
                            }else{
                              $numero_identificacao = $item['cnpj'];  
                            }
                            
                            if($item['nome_foto'] != null){
                                $foto = 'http://' . ''. $_SERVER['HTTP_HOST']. $item['caminho_foto'] . $item['nome_foto'];
                                echo '<td><img src = "' . "$foto" . '" style="width: 100px;"/>';
                            }else{
                                $no_image = 'http://' . ''. $_SERVER['HTTP_HOST']. '/arca/protected/arquivos/noimagens/noimagem.png"';
                                echo '<td><img src = "' . "$no_image" . '" style="width: 100px;"/>';  
                            }
                            echo '<td class="alinhamento_td">' . $nome_cliente;
                            echo '<td class="alinhamento_td">' . $item['descricao_categoria'];
                            echo '<td class="alinhamento_td">' . $registro;
                            echo '<td class="alinhamento_td">' . $numero_identificacao;
                            echo '<td class="alinhamento_td">' . $item['endereco_cliente'];
                            echo '<td class="alinhamento_td">' . $item['telefone'];
                            echo '<td class="alinhamento_td">' . $item['celular'];
                            if($item['nome_arquivo'] != null){
                                $arquivo = 'http://' . ''. $_SERVER['HTTP_HOST']. $item['caminho_arquivo'] . $item['nome_arquivo'];
                                echo '<td class="alinhamento_td">' . '<a href="'.$arquivo.'" download title="Clique para fazer download">Documento</a>';
                            }else{
                                $urlbase = 'http://' . ''. $_SERVER['HTTP_HOST']. '/arca/protected/imagens/noimagens/noimagem.png"';
                                echo '<td class="alinhamento_td">Não possui Documento';  
                            }
                            
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
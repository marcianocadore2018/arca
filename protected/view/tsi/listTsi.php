<div id="fundo">
    <div class="">
        <div class="panel panel-primary">
            <div class="panel-heading">Relação de TSI</div>
            <div class="panel-body">
                    <a href="index.php?controle=tsiController&acao=novo">
                        <span class='glyphicon glyphicon-plus'> Adicionar</span>
                    </a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>Nome</th>
                    <th>Descrição</th>
                    <th>Valor</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($listaDados as $item) {
                                echo '<tr>';
                                $id = $item['id'];
                                echo '<td>' . $item['nome'];
                                echo '<td>' . $item['descricao'];
                                echo '<td>' . $item['preco'];

                                $string = 'passar' . $id . 'metodoget';
                                $idencriptografa = base64_encode($string);
                                echo "<td> <a href='index.php?controle=tsiController&acao=buscar&id=$idencriptografa'>"
                                . " <span class='glyphicon glyphicon-pencil'> </span>"
                                . "</a> </td>";
                                echo "<td> <a onclick='excluir(\"excluir\",\"tsiController\",\"$idencriptografa\")' href='#'>"
                                . " <span class='glyphicon glyphicon-trash customDialog'> </span>"
                                . "</a> </td>";

                                echo '</tr>';
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
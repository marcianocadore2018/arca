<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de TSI</div>
        <div class="panel-body">
            <form action="<?= $acao ?>" name="formTsi" id="formTsi" method="POST" class="form" role="form">
                <input type="hidden" class="form-control" id="id" name="id" readonly="true" value="<?php if (isset($tsi)) echo $tsi['id']; ?>">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($tsi)) echo $tsi['id']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="nome">Nome</label>
                        <input type="text" class="form-control" id="nome" name="nome" placeholder="Digite o Nome da TSI" 
                               value="<?php if (isset($tsi)) echo $tsi['nome']; ?>" required minlength="3" maxlength="100">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="descricao">Descrição</label>
                        <textarea class="form-control" id="descricao" name="descricao"><?php if (isset($tsi)) echo $tsi['descricao']; ?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1">
                        <label for="preco">Preço do Produto</label>
                        <input type="text" class="form-control text-right" id="valor" name="preco" placeholder="R$ 0,00" 
                               value="<?php if (isset($tsi)) echo $tsi['preco']; ?>" required maxlength="16">
                    </div>
                </div>
                
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>

<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
$("#formTsi").validate({
    rules: {
        nome: {
            required: true
        },
        descricao: {
            required: true
        },
        preco: {
            required: true
        }
    },
    messages: {
        nome: {
            required: "Por favor, informe a Nome",
            minlength: "O Nome deve ter pelo menos 3 caracteres",
            maxlength: "O Nome deve ter no máximo 200 caracteres"
        },
        descricao: {
            required: "Por favor, informe a Descrição"
        },
        preco: {
            required: "Por favor, informe o Preço"
        }
    }
});
</script>
<div id="fundo">
    <div class="">
        <div class="panel panel-primary">
            <div class="panel-heading">Relação de Frete</div>
            <div class="panel-body">
                    <a href="index.php?controle=freteController&acao=novo">
                        <span class='glyphicon glyphicon-plus'> Adicionar</span>
                    </a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>Origem</th>
                    <th>Destino</th>
                    <th>Valor</th>
                    <th>Data Frete</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($listaDados as $item) {
                                echo '<tr>';
                                $id = $item['id'];
                                echo '<td>' . $item['origem'];
                                echo '<td>' . $item['destino'];
                                echo '<td>' . "R$ " . $item['valor'];
                                echo '<td>' . $item['datafrete'];

                                $string = 'passar' . $id . 'metodoget';
                                $idencriptografa = base64_encode($string);
                                echo "<td> <a href='index.php?controle=freteController&acao=buscar&id=$idencriptografa'>"
                                . " <span class='glyphicon glyphicon-pencil'> </span>"
                                . "</a> </td>";
                                echo "<td> <a onclick='excluir(\"excluir\",\"freteController\",\"$idencriptografa\")' href='#'>"
                                . " <span class='glyphicon glyphicon-trash customDialog'> </span>"
                                . "</a> </td>";

                                echo '</tr>';
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
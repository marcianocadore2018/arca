<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Frete</div>
        <div class="panel-body">
            <form action="<?= $acao ?>" name="formFrete" id="formFrete" method="POST" class="form" role="form">
                <input type="hidden" class="form-control" id="id" name="id" readonly="true" value="<?php if (isset($frete)) echo $frete['id']; ?>">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($frete)) echo $frete['id']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="origem">Origem</label>
                        <input type="text" class="form-control" id="origem" name="origem" placeholder="Digite o Origem" 
                               value="<?php if (isset($frete)) echo $frete['origem']; ?>" required minlength="3" maxlength="100">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label for="destino">Destino</label>
                        <input type="text" class="form-control" id="destino" name="destino" placeholder="Digite o Destino" 
                               value="<?php if (isset($frete)) echo $frete['destino']; ?>" required minlength="3" maxlength="100">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label for="valor">Valor do Frete</label>
                        <input type="text" class="form-control text-right" id="valor" name="valor" placeholder="R$ 0,00" 
                               value="<?php if (isset($frete)) echo $frete['valor']; ?>" required maxlength="16">
                    </div>
                </div>
                
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>

<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
$("#formFrete").validate({
    rules: {
        origem: {
            required: true
        },
        destino: {
            required: true
        },
        valor: {
            required: true
        }
    },
    messages: {
        origem: {
            required: "Por favor, informe a Origem"
        },
        destino: {
            required: "Por favor, informe o Destino"
        },
        valor: {
            required: "Por favor, informe o Preço"
        }
    }
});
</script>
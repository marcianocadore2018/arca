<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Cidade</div>
        <div class="panel-body">
            <form action="<?= $acao ?>" name="formCidade" id="formCidade" method="POST" class="form" role="form">
                <input type="hidden" class="form-control" id="id" name="id" readonly="true" value="<?php if (isset($cidade)) echo $cidade['id']; ?>">
                
                <div class="row">
                    <div class="col-md-8">
                        <label for="nomecidade">Nome</label>
                        <input type="text" class="form-control" id="nomecidade" name="nomecidade" placeholder="Digite o Nome da Cidade" 
                               value="<?php if (isset($cidade)) echo $cidade['nomecidade']; ?>" required minlength="3" maxlength="100">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <label for="idestado">UF</label>
                        <select class="form-control" name="idestado" id="idestado" required data-errormessage-value-missing="Selecione o Estado">
                            <option value=''>Selecione o Estado</option>
                            <?php
                            foreach ($listaEstados as $estados) {
                                $selected = (isset($cidade) && $cidade['idestado'] == $estados['id']) ? 'selected' : '';
                                ?>
                                <option value='<?php echo $estados['id']; ?>'
                                        <?php echo $selected; ?>> 
                                            <?php echo $estados['uf']; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>

<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
$("#formCidade").validate({
    rules: {
        nomecidade: {
            required: true
        },
        idestado: {
            required: true
        }
    },
    messages: {
        nomecidade: {
            required: "Por favor, informe o Nome da Cidade",
            minlength: "O Nome da cidade deve ter pelo menos 3 caracteres",
            maxlength: "O Nome da cidade deve ter no máximo 300 caracteres"
        },
        idestado: {
            required: "Por favor, Selecione o Estado"
        }
    }
});
</script>
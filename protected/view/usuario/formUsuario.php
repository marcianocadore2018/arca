<div class="col-md-12 col-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">Cadastro de Usuário</div>
        <div class="panel-body">
            <form action="<?= $acao ?>" name="formUsuario" id="formUsuario" method="POST" class="form" role="form">
                <input type="hidden" class="form-control" id="id" name="id" readonly="true" value="<?php if (isset($usuario)) echo $usuario['id']; ?>">
                <div class="row">
                    <div class="col-md-1">
                        <label for="id">Id</label>
                        <input type="text" class="form-control" id="id" name="id" readonly="true" 
                               value="<?php if (isset($usuario)) echo $usuario['id']; ?>">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <label for="nomeusuario">Nome Usuário</label>
                        <input type="text" class="form-control" id="nomeusuario" name="nomeusuario" placeholder="Digite o Nome do USuário" 
                               value="<?php if (isset($usuario)) echo $usuario['nomeusuario']; ?>" required minlength="3" maxlength="100">
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-8">
                        <label for="login">Login</label>
                        <input type="text" class="form-control" id="login" name="login" placeholder="Digite o Login" 
                               value="<?php if (isset($usuario)) echo $usuario['login']; ?>" required minlength="3" maxlength="20">
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-8">
                        <label for="senhausuario">Senha Usuário</label>
                        <input type="password" class="form-control" id="senhausuario" name="senhausuario" placeholder="Digite a Senha" 
                               value="<?php if (isset($usuario)) echo $usuario['senhausuario']; ?>" required minlength="3" maxlength="20">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <label for="tipousuario">Tipo Usuário</label>
                        <br>
                        <input type="radio" name="tipousuario" value="M" 
                        <?php
                        if (isset($usuario))
                            if ($usuario['tipousuario'] == 'M') {
                                echo 'checked';
                            } else {
                                $usuario == null;
                            }
                        ?>> Diretor
                        
                        <input type="radio" name="tipousuario" value="G"
                        <?php
                        if (isset($usuario))
                            if ($usuario['tipousuario'] == 'G') {
                                echo 'checked';
                            } else {
                                $usuario == null;
                            }
                        ?>> Gerente
                        <input type="radio" name="tipousuario" value="C" 
                        <?php
                        if (isset($usuario))
                            if ($usuario['tipousuario'] == 'C') {
                                echo 'checked';
                            } else {
                                $usuario == null;
                            }
                        ?>> Colaborador
                    </div>
                    <br/>
                </div>
                <br/>
                <button type="submit" class="btn btn-success">Gravar</button>
                <button type="reset" class="btn btn-primary">Limpar</button>
            </form>
        </div>
    </div>
</div>

<script src="includes/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="includes/js/jquery.validate.min.js" type="text/javascript"></script>

<script>
$("#formUsuario").validate({
    rules: {
        nomeusuario: {
            required: true
        },
        login: {
            required: true
        },
        senhausuario: {
            required: true
        },
        tipousuario: {
            required: true
        }
    },
    messages: {
        nomeusuario: {
            required: "Por favor, informe o Nome do Usuário",
            minlength: "O Nome do usuário deve ter pelo menos 3 caracteres",
            maxlength: "O Nome do usuário deve ter no máximo 100 caracteres"
        },
        login: {
            required: "Por favor, informe o Login",
            minlength: "O Login do usuário deve ter pelo menos 5 caracteres",
            maxlength: "O Login do usuário deve ter no máximo 20 caracteres"
        },
        senhausuario: {
            required: "Por favor, informe a Senha do Usuário",
            minlength: "A Senha do usuário deve ter pelo menos 3 caracteres",
            maxlength: "A Senha do usuário deve ter no máximo 20 caracteres"
        },
        tipousuario: {
            required: "Por favor, informe o Tipo do Usuário"
        }
    }
});
</script>
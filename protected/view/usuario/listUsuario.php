<div id="fundo">
    <div class="">
        <div class="panel panel-primary">
            <div class="panel-heading">Relação de Usuários</div>
            <div class="panel-body">
                    <a href="index.php?controle=usuarioController&acao=novo">
                        <span class='glyphicon glyphicon-plus'> Adicionar</span>
                    </a>
            </div>
            <div class="table-responsive">
                <table class="table" id="example1">
                    <thead>
                    <th>Nome Usuário</th>    
                    <th>Login</th>
                    <th>Data Cadastro</th>
                    <th>Tipo Usuário</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($listaDados as $item) {
                                echo '<tr>';
                                $id = $item['id'];
                                echo '<td>' . $item['nomeusuario'];
                                echo '<td>' . $item['login'];
                                echo '<td>' . $item['datacadastro'];
                                if($item['tipousuario'] == 'G'){
                                  $tipousuario = 'Gerente';
                                }else if($item['tipousuario'] == 'C'){
                                  $tipousuario = 'Colaborador';
                                }else{
                                  $tipousuario = 'Diretor Proprietário';
                                }
                                echo '<td>' . $tipousuario;
                                
                                $string = 'passar' . $id . 'metodoget';
                                $idencriptografa = base64_encode($string);
                                echo "<td> <a href='index.php?controle=usuarioController&acao=buscar&id=$idencriptografa'>"
                                . " <span class='glyphicon glyphicon-pencil'> </span>"
                                . "</a> </td>";
                                echo "<td> <a onclick='excluir(\"excluir\",\"usuarioController\",\"$idencriptografa\")' href='#'>"
                                . " <span class='glyphicon glyphicon-trash customDialog'> </span>"
                                . "</a> </td>";

                                echo '</tr>';
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
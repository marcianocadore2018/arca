<?php

class TsiModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $nome_tsi = strtolower($_POST['nome']);

        $formata      = str_replace("R$", "", str_replace("", "", $dados['preco']));
        $formatavalor = str_replace(".", "", str_replace("", "", $formata));
        $formatavalor = str_replace(",", ".", str_replace("", "", $formatavalor));
        
        //Verifica Descrição
        $sql = "SELECT nome FROM tsi WHERE nome = " . "'" . $nome_tsi . "'";
        $sql = $this->bd->prepare($sql);
        $sql->execute();
        
        if ($sql->rowCount() > 0) {
            return $sql->errorInfo();
        }else{
            $sql = "INSERT INTO tsi(nome, descricao, preco) VALUES('$nome_tsi', :descricao, $formatavalor)";
        }
        unset($dados['id']);
        unset($dados['nome']);
        unset($dados['preco']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }
    

    public function buscarTodos() {
        $sql = "select id, upper(nome) as nome, descricao, preco from tsi order by nome asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
            $sql = "SELECT id, upper(nome) as nome, descricao, preco FROM tsi WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        //Valor formatação
        $id = $dados['id'];
        $nome_tsi = strtolower($_POST['nome']);
        $formata      = str_replace("R$", "", str_replace("", "", $dados['preco']));
        //Valor formatação
        $valorproduto = $formata;
        $verificavalorproduto = substr($valorproduto, -3, 1);
        if($verificavalorproduto == ","){
            $formata      = str_replace("R$", "", str_replace("", "", $valorproduto));
            $formatavalor = str_replace(".", "", str_replace("", "", $formata));
            $formatavalor = str_replace(",", ".", str_replace("", "", $formatavalor));
            $precoprodutoformatado   = $formatavalor;
        }else{
            $valorproduto = $dados['preco'];
            $formata      = str_replace("R$", "", str_replace("", "", $valorproduto));
            $removevirgula = str_replace(",", "", $formata);
            $precoprodutoformatado = $removevirgula;
        }

        $sql = "SELECT nome FROM tsi WHERE nome = " . "'" . $nome_tsi . "' AND id <> $id";
        $sql = $this->bd->prepare($sql);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            return $sql->errorInfo();
        }else{
            $update_sql = "UPDATE tsi SET nome = '$nome_tsi', descricao = :descricao, preco = $precoprodutoformatado WHERE id = $id";
            unset($dados['id']);
            unset($dados['nome']);
            unset($dados['preco']);
            $query = $this->bd->prepare($update_sql);
            return $query->execute($dados);
        }
    }

    public function excluir($id) {
        $sqlcategoria = "delete from tsi where id = $id";
        $query = $this->bd->prepare($sqlcategoria);
        return $query->execute();
    }

}

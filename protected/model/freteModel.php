<?php

class FreteModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $formata      = str_replace("R$", "", str_replace("", "", $dados['valor']));
        $formatavalor = str_replace(".", "", str_replace("", "", $formata));
        $formatavalor = str_replace(",", ".", str_replace("", "", $formatavalor));
        $data = date('d/m/Y');
        $hora = date('H:i');
        $datafrete = ($data . ' - '. $hora);
        $sql = "INSERT INTO frete(origem, destino, valor, datafrete) VALUES(:origem, :destino, $formatavalor, '$datafrete')";
        unset($dados['id']);
        unset($dados['valor']);
        unset($dados['datafrete']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }
    

    public function buscarTodos() {
        $sql = "SELECT id, origem, destino, valor, datafrete FROM frete order by datafrete asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "SELECT id, origem, destino, valor, datafrete FROM frete WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        //Valor formatação
        $id = $dados['id'];

        $formata      = str_replace("R$", "", str_replace("", "", $dados['valor']));
        //Valor formatação
        $valorfrete = $formata;
        $verificafrete = substr($valorfrete, -3, 1);
        if($verificafrete == ","){
            $formata      = str_replace("R$", "", str_replace("", "", $valorfrete));
            $formatavalor = str_replace(".", "", str_replace("", "", $formata));
            $formatavalor = str_replace(",", ".", str_replace("", "", $formatavalor));
            $valorfreteformatado   = $formatavalor;
        }else{
            $valorfrete = $dados['valor'];
            $formata      = str_replace("R$", "", str_replace("", "", $valorfrete));
            $removevirgula = str_replace(",", "", $formata);
            $valorfreteformatado = $removevirgula;
        }
        
        $update_sql = "UPDATE frete 
                         SET origem = :origem, 
                             destino = :destino, 
                             valor = $valorfreteformatado 
                       WHERE id = $id";
        unset($dados['id']);
        unset($dados['valor']);
        unset($dados['datafrete']);
        $query = $this->bd->prepare($update_sql);
        return $query->execute($dados);
    }

    public function excluir($id) {
        $sqlfrete = "delete from frete where id = $id";
        $query = $this->bd->prepare($sqlfrete);
        return $query->execute();
    }

}

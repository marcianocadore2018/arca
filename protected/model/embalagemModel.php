<?php

class EmbalagemModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $descricao_embalagem = strtolower($_POST['descricao']);
        
        //Verifica Descrição
        $sql = "SELECT descricao FROM embalagens WHERE descricao = " . "'" . $descricao_embalagem . "'";
        $sql = $this->bd->prepare($sql);
        $sql->execute();
        
        if ($sql->rowCount() > 0) {
            return $sql->errorInfo();
        }else{
            $sql = "INSERT INTO embalagens(descricao) VALUES('$descricao_embalagem')";
        }
        unset($dados['id']);
        unset($dados['descricao']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }
    

    public function buscarTodos() {
        $sql = "select id, upper(descricao) as descricao from embalagens order by descricao asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "SELECT id, upper(descricao) as descricao FROM embalagens WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        //Valor formatação
        $id = $dados['id'];
        $descricao_embalagem = strtolower($_POST['descricao']);

        $sql = "SELECT descricao FROM embalagens WHERE descricao = " . "'" . $descricao_embalagem . "' and id <> $id";
        $sql = $this->bd->prepare($sql);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            return $sql->errorInfo();
        }else{
            $update_sql = "UPDATE embalagens SET descricao = '$descricao_embalagem' WHERE id = $id";
            unset($dados['id']);
            unset($dados['descricao']);
            $query = $this->bd->prepare($update_sql);
            return $query->execute($dados);
        }
    }

    public function excluir($id) {
        $sqlembalagem = "delete from embalagens where id = $id";
        $query = $this->bd->prepare($sqlembalagem);
        return $query->execute();
    }

}

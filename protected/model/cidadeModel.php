<?php

class CidadeModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $nome = strtolower($_POST['nomecidade']);
        $idestado = $_POST['idestado'];
        
        //Verifica Descrição
        $sql = "SELECT nome FROM cidade WHERE nome = '" . $nome . "' AND idestado = '". $idestado ."'";
        $sql = $this->bd->prepare($sql);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            return $sql->errorInfo();
        }else{
            $sql = "INSERT INTO cidade(nome, idestado) VALUES('$nome', $idestado)";
            unset($dados['id']);
            unset($dados['nomecidade']);
            unset($dados['idestado']);
            $query = $this->bd->prepare($sql);
            return $query->execute($dados);
        }
    }
    

    public function buscarTodos() {
        $sql = "select ci.id as id,
                       initcap(ci.nome) as nomecidade,
                       est.uf as uf 
                  from cidade ci
                 inner join estado est
                    on ci.idestado = est.id
              order by nomecidade, uf; ";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        $sql = "select ci.id as id,
                       ci.nome as nomecidade,
                       est.uf as uf,
                       est.id as idestado
                  from cidade ci
                 inner join estado est
                    on ci.idestado = est.id
                 where ci.id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $id = $_POST['id'];
        $nome = strtolower($_POST['nomecidade']);
        $idestado = $_POST['idestado'];
        
        $sql = "SELECT nome FROM cidade WHERE nome = '" . $nome . "' AND idestado = '". $idestado ."' AND id <> $id";
        
        $sql = $this->bd->prepare($sql);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            return $sql->errorInfo();
        }else{
            $update_sql = "update cidade set nome = '$nome', idestado = $idestado WHERE id = $id";
        
            unset($dados['id']);
            unset($dados['nomecidade']);
            unset($dados['idestado']);
            $query = $this->bd->prepare($update_sql);
            return $query->execute($dados);
        }
    }

    public function excluir($id) {
        $sqlcidade = "delete from cidade where id = $id";
        $query = $this->bd->prepare($sqlcidade);
        return $query->execute();
    }

}

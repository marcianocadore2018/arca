﻿<?php

class VendedorModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        //Pegar o diretorio do projeto para o arquivo
        $diretorio = $_SERVER['DOCUMENT_ROOT'] . '/arca/protected/arquivos/uploads/';
        $caminhodiretorioarquivo = 'arca/protected/arquivos/uploads/';
        
        //Pegar o diretorio do projeto para a imagem
        $diretoriofoto = $_SERVER['DOCUMENT_ROOT'] . '/arca/protected/arquivos/imagens/';
        $caminhodiretoriofoto = 'arca/protected/arquivos/imagens/';
        $arquivo = $_FILES['arquivo']['name'];
        $nomearquivoaleatorio = $arquivo;
        
        $foto = $_FILES['foto']['name'];
        $nomefotoaleatorio = $foto;

        //se houver algum ponto ou virgula remover por nada
        //str_shuffle gera um nome aleatório
        $formatadonome = preg_replace('/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $nomearquivoaleatorio));
        $formatafotonome = preg_replace('/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $nomefotoaleatorio));
        
        $nomealeatorio = str_shuffle($formatadonome);
        $nomealeatoriofoto = str_shuffle($formatafotonome);
        
        $nomearquivo = str_replace(".", "", $nomealeatorio); // Primeiro tira os pontos
        $arquivo_tmp = $_FILES['arquivo']['tmp_name'];
        
        $nomefoto = str_replace(".", "", $nomealeatoriofoto); // Primeiro tira os pontos
        $foto_tmp = $_FILES['foto']['tmp_name'];
        
        //Pega extensão do arquivo
        $extensaoarquivo = substr($arquivo, -4);
        $destino = $diretorio . $nomearquivo . $extensaoarquivo;
        
        //Pega extensão da foto
        $extensaofoto = substr($foto, -4);
        $destinofoto = $diretoriofoto . $nomefoto . $extensaofoto;
        
        //alterar extensão de imagem para minúscula
        $res = strtolower($extensaoarquivo);
        $resfoto = strtolower($extensaofoto);
        
        #
        move_uploaded_file($arquivo_tmp, $destino);
        #Enviando a foto
        move_uploaded_file($foto_tmp, $destinofoto);
          
        //Gravar as informa~ções do diretório do arquivo
        $caminhoarquivo = $diretorio;
        //Gravar as informa~ções do nome do arquivo
        $nomearquivoaleatorio = $nomealeatorio;
          
        //Gravar as informa~ções do diretório da foto
        $caminhofoto = $diretoriofoto;
          
        //Gravar as informa~ções do nome da foto
        $nomefotoaleatoriofoto = $nomealeatoriofoto;
          
          
        #ARQUIVO
        $nomearquivoaleatoriosemponto = str_replace(".", "", $nomearquivoaleatorio);
        $nomearquivo = $nomearquivoaleatoriosemponto . $res;
          
        #FOTO
        $nomefotoaleatoriosemponto = str_replace(".", "", $nomefotoaleatoriofoto);
        $nomefoto = $nomefotoaleatoriosemponto . $resfoto;
          
        #}
        
        $tipopessoa = $_POST['tipopessoa'];
        $observacao = $_POST['observacao'];
        $situacaovendedor = isset($_POST['situacaovendedor']);
        $endereco = $_POST['endereco'];
        $conta_deposito = $_POST['conta_deposito'];
        $agencia_deposito = $_POST['agencia_deposito'];
        $bairro = $_POST['bairro'];
        $numero = $_POST['numero'];
        $cidade = $_POST['idcidade'];
        $idestado = $_POST['idestado'];
        $idcategoria = $_POST['idcategoria'];
        $cep = $_POST['cep'];
        $complemento = $_POST['complemento'];
        $telefone = $_POST['telefone'];
        $celular = $_POST['celular'];
        $contato = $_POST['contato'];
        $email = $_POST['email'];
        $skype = $_POST['skype'];
        $razao_social = $_POST['razao_social'];
        
        if ($_FILES['arquivo']['name'] != ""){
           $nomearquivo == $nomearquivo;
           $caminhoarquivo = $caminhoarquivo;
        }else{
           $nomearquivo = "";
           $caminhoarquivo = "";
        }
        
        if ($tipopessoa == 'PF') {
            $nome = $_POST['nome'];
            $datanascimento = $_POST['datanascimento'];
            $sexo = $_POST['sexo'];
            $rg = $_POST['rg'];
            $cpf = $_POST['cpf'];
            //Verifica CPF
            $sql = "SELECT cpf FROM vendedor WHERE cpf = '" . $cpf . "'";

            $sql = $this->bd->prepare($sql);
            $sql->execute();
            
            if ($sql->rowCount() > 0) {
                return "CPF do vendedor já existente. Favor informar outro CPF!";
            }else{
                
                $sql = "INSERT INTO vendedor(nome, idcategoria, tipo_pessoa, datanascimento, conta_deposito, agencia_deposito, genero, rg, cpf, observacao, endereco, bairro, numero, idcidade, idestado, cep, complemento, telefone, celular, contato, email, skype, nome_arquivo, caminho_arquivo, nome_foto, caminho_foto) "
                        . " VALUES('$nome', $idcategoria, '$tipopessoa', '$datanascimento', '$conta_deposito', '$agencia_deposito', '$sexo', '$rg', '$cpf', '$observacao', '$endereco', '$bairro', $numero, $cidade, $idestado, '$cep', '$complemento', '$telefone', '$celular', '$contato', '$email', '$skype', '$nomearquivo', '$caminhodiretorioarquivo', '$nomefoto', '$caminhodiretoriofoto')";
            }
        } else {
            $cnpj = $_POST['cnpj'];
            $inscricao_estadual = $_POST['inscricao_estadual'];
            $nome_representante = isset($_POST['nome_representante']);
            
            //Verifica CNPJ
            $sql = "SELECT cnpj FROM vendedor WHERE cpf = '" . $cnpj . "'";
            $sql = $this->bd->prepare($sql);
            $sql->execute();
            
            if ($sql->rowCount() > 0) {
                return "CNPJ do vendedor já existente. Favor informar outro CNPJ!";
            }else{
                $sql = "INSERT INTO vendedor(tipo_pessoa, idcategoria, razao_social, cnpj, conta_deposito, agencia_deposito, inscricao_estadual, nome_representante, observacao, endereco, bairro, numero, idcidade, idestado, cep, complemento, telefone, celular, contato, email, skype, nome_arquivo, caminho_arquivo, nome_foto, caminho_foto) "
                            . " VALUES('$tipopessoa', $idcategoria, '$razao_social','$cnpj', '$conta_deposito', '$agencia_deposito', '$inscricao_estadual', '$nome_representante', '$observacao', '$endereco', '$bairro', $numero, $cidade, $idestado, '$cep', '$complemento', '$telefone', '$celular', '$contato', '$email', '$skype', '$nomearquivo', '$caminhodiretorioarquivo', '$nomefoto', '$caminhodiretoriofoto')";
            }
        }
        unset($dados['id']);
        unset($dados['nome']);
        unset($dados['tipopessoa']);
        unset($dados['tipo_pessoa']);
        unset($dados['datanascimento']);
        unset($dados['razao_social']);
        unset($dados['conta_deposito']);
        unset($dados['agencia_deposito']);
        unset($dados['sexo']);
        unset($dados['rg']);
        unset($dados['cpf']);
        unset($dados['observacao']);
        unset($dados['endereco']);
        unset($dados['bairro']);
        unset($dados['numero']);
        unset($dados['idcidade']);
        unset($dados['idestado']);
        unset($dados['idcategoria']);
        unset($dados['cep']);
        unset($dados['complemento']);
        unset($dados['telefone']);
        unset($dados['celular']);
        unset($dados['email']);
        unset($dados['skype']);
        unset($dados['arquivo']);
        unset($dados['caminho_arquivo']);
        unset($dados['cnpj']);
        unset($dados['inscricao_estadual']);
        unset($dados['contato']);
        unset($dados['nome_representante']);
        
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }
    

    public function buscarTodos() {
        $sql = "select ve.id as id,
                       ve.tipo_pessoa as tipopessoa, 
                       ve.nome as nomevendedor, 
                       to_char(ve.datanascimento, 'dd/MM/yyyy') as datanascimento, 
                       ve.genero as genero, 
                       cat.descricao as descricao_categoria,
                       ve.rg as rg, 
                       ve.cpf as cpf,
                       ve.conta_deposito as conta_deposito, 
                       ve.agencia_deposito as agencia_deposito,
                       ve.razao_social as razao_social,
                       ve.cnpj as cnpj,
                       ve.inscricao_estadual as inscricaoestadual,
                       (initcap(cid.nome) || ' - ' || ve.endereco || ' - ' || ve.bairro || ' - ' || ve.numero || ' - ' || ve.cep || ' - ' || est.uf) as endereco_vendedor,
                       ve.telefone as telefone, 
                       ve.celular as celular, 
                       ve.email as email, 
                       ve.skype as skype,
                       (ve.caminho_arquivo || ve.nome_arquivo) as arquivo,
                       ('/' || ve.caminho_arquivo) as caminho_arquivo,
                       ve.nome_arquivo as nome_arquivo,
                       ('/' || ve.caminho_foto || ve.nome_foto) as foto,
                       ('/' || ve.caminho_foto) as caminho_foto,
                       ve.nome_foto as nome_foto
                  from vendedor ve
                 inner join estado est 
                    on ve.idestado = est.id
                 inner join categoria cat
                    on ve.idcategoria = cat.id
                 inner join cidade cid
                    on cid.id = ve.idcidade
                 order by ve.nome asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        if (isset($_SERVER['HTTPS'])) {
            $prefixo = 'https://';
        } else {
            $prefixo = 'http://';
        }

        //URL BASE
        $urlbase = $prefixo . '' . $_SERVER['HTTP_HOST'] . '/';
        
        $sql = "SELECT ve.id as id,
                       ve.nome as nome,
                       to_char(ve.datanascimento, 'dd/MM/yyyy') as datanascimento,
                       ve.rg as rg,
                       ve.razao_social as razao_social,
                       ve.cpf as cpf,
                       ve.conta_deposito as conta_deposito, 
                       ve.agencia_deposito as agencia_deposito,
                       ve.cnpj as cnpj,
                       ve.inscricao_estadual as inscricao_estadual,
                       ve.nome_representante as nome_representante,
                       ve.genero as sexo,
                       ve.observacao as observacao,
                       ve.endereco as endereco,
                       ve.numero as numero, 
                       ve.bairro as bairro,
                       ve.idcidade as idcidade,
                       ve.cep as cep,
                       ve.complemento as complemento,
                       ve.telefone as telefone,
                       ve.celular as celular,
                       ve.contato as contato,
                       ve.email as email,
                       ve.skype as skype,
                       ve.idestado as idestado,
                       ve.idcategoria as idcategoria,
                       ve.tipo_pessoa as tipo_pessoa,
                       (ve.caminho_arquivo || ve.nome_arquivo) as arquivo,
                       ve.nome_arquivo as nome_arquivo,
                       (ve.caminho_foto || ve.nome_foto) as foto,
                       ve.nome_foto as nome_foto
                  FROM vendedor ve
                 INNER JOIN estado est 
                    on ve.idestado = est.id
                 INNER JOIN categoria cat
                    on ve.idcategoria = cat.id
                 INNER JOIN cidade cid
                    on ve.idcidade = cid.id
                 WHERE ve.id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $diretorio = $_SERVER['DOCUMENT_ROOT'] . '/arca/protected/arquivos/uploads/';
        $caminhodiretorio = 'arca/protected/arquivos/uploads/';

        #FOTO
        $diretoriofoto = $_SERVER['DOCUMENT_ROOT'] . '/arca/protected/arquivos/imagens/';
        $caminhodiretoriofoto = 'arca/protected/arquivos/imagens/';
        
        //Consulta se nome da imagem ja existe e exclui a imagem do diretório se o usuário alterar um registro 
        //que ja possui o mesmo nome da imagem
        $consultaarquivo = "select caminho_arquivo, nome_arquivo from vendedor where id = " . $_POST['id'];
        $sqlconsultaarquivo = $this->bd->prepare($consultaarquivo);
        $sqlconsultaarquivo->execute();


        if ($_FILES['arquivo']['name'] != '') {
            $arquivo = $_FILES['arquivo']['name'];
            $arquivoaleatorio = $arquivo;
            $formatacao = preg_replace('/[`^~\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $arquivoaleatorio));
            $nomealeatorio = str_shuffle($formatacao);

            //se houver algum ponto ou virgula remover por nada
            $nomearquivo = str_replace(".", "", $nomealeatorio);
            $arquivo_tmp = $_FILES['arquivo']['tmp_name'];

            $extensaoarquivo = substr($arquivo, -4);

            //alterar extensão de imagem para minúscula
            $destino = $diretorio . $nomearquivo . $extensaoarquivo;
            //Converter a extensão para letra minúscula
            $res = strtolower($extensaoarquivo);

            //Verificar se o arquivo esta indo vazio
            if ($res != '') {
                $arquivo_tmp = $_FILES['arquivo']['tmp_name'];
                move_uploaded_file($arquivo_tmp, $destino);
                //Gravar essas informações no banco, vai ter o caminho e o nome da foto.
                $caminho_arquivo = $caminhodiretorio;
                $nomearquivoaleatorio = $nomealeatorio;
                $nomearquivoaleatoriosemponto = str_replace(".", "", $nomearquivoaleatorio);
                $nomearquivo = $nomearquivoaleatoriosemponto . $extensaoarquivo;
            }
        } else {
            if ($sqlconsultaarquivo->rowCount() > 0) {
                foreach ($sqlconsultaarquivo as $rs) {
                    $nomearquivo = $rs["nome_arquivo"];
                    $caminho_arquivo = $rs["caminho_arquivo"];
                }
            }
        }
        
        //Consulta se nome da imagem ja existe e exclui a imagem do diretório se o usuário alterar um registro 
        //que ja possui o mesmo nome da imagem
        $consultafoto = "select caminho_foto, nome_foto from vendedor where id = " . $_POST['id'];
        $sqlconsultafoto = $this->bd->prepare($consultafoto);
        $sqlconsultafoto->execute();
        
        if ($_FILES['foto']['name'] != '') {
            #FOTO
            $foto = $_FILES['foto']['name'];
            $fotoaleatorio = $foto;
            $formatacaofoto = preg_replace('/[`^~\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $fotoaleatorio));
            $nomefotoaleatorio = str_shuffle($formatacaofoto);

            //FOTO se houver algum ponto ou virgula remover por nada
            $nomefoto = str_replace(".", "", $nomefotoaleatorio);
            $foto_tmp = $_FILES['foto']['tmp_name'];
            $extensaofoto = substr($foto, -4);

            //FOTO
            //alterar extensão de imagem para minúscula
            $destinofoto = $diretoriofoto . $nomefoto . $extensaofoto;
            //Converter a extensão para letra minúscula
            $resfoto = strtolower($extensaofoto);

            //Verificar se o arquivo esta indo vazio
            if ($resfoto != '') {
                #FOTO
                $foto_tmp = $_FILES['foto']['tmp_name'];
                move_uploaded_file($foto_tmp, $destinofoto);
                $caminho_foto = $caminhodiretoriofoto;
                $nomefotoaleatorio = $nomefotoaleatorio;
                $nomefotoaleatoriosemponto = str_replace(".", "", $nomefotoaleatorio);
                $nomefoto = $nomefotoaleatoriosemponto . $extensaofoto;
            }
        } else {
            if ($sqlconsultafoto->rowCount() > 0) {
                foreach ($sqlconsultafoto as $rs) {
                    $nomefoto = $rs["nome_foto"];
                    $caminho_foto = $rs["caminho_foto"];
                }
            }
        }
        
        $id = $_POST['id'];
        $observacao = $_POST['observacao'];
        $endereco = $_POST['endereco'];
        $bairro = $_POST['bairro'];
        $numero = $_POST['numero'];
        $cidade = $_POST['idcidade'];
        $conta_deposito = $_POST['conta_deposito'];
        $agencia_deposito = $_POST['agencia_deposito'];
        $idestado = $_POST['idestado'];
        $idcategoria = $_POST['idcategoria'];
        $cep = $_POST['cep'];
        $complemento = $_POST['complemento'];
        $telefone = $_POST['telefone'];
        $celular = $_POST['celular'];
        $contato = $_POST['contato'];
        $email = $_POST['email'];
        $skype = $_POST['skype'];

        if ($_POST['tipopessoa'] == 'PF'){
            $nome = $_POST['nome'];
            $datanascimento = $_POST['datanascimento'];
            $sexo = $_POST['sexo'];
            $rg = $_POST['rg'];
            $cpf = $_POST['cpf'];
            
            $update_sql = "update vendedor set nome = '$nome',
                                              datanascimento = '$datanascimento',
                                              genero = '$sexo',
                                              rg = '$rg',
                                              cpf = '$cpf',
                                              conta_deposito = '$conta_deposito',
                                              agencia_deposito = '$agencia_deposito',    
                                              observacao = '$observacao',
                                              endereco = '$endereco',
                                              bairro = '$bairro',
                                              numero = $numero,
                                              idcidade = $cidade,
                                              idestado = $idestado,
                                              idcategoria = $idcategoria,
                                              cep = '$cep',
                                              complemento = '$complemento',
                                              telefone = '$telefone',
                                              celular = '$celular',
                                              contato = '$contato',
                                              email = '$email',
                                              skype = '$skype',
                                              nome_arquivo = '$nomearquivo',
                                              caminho_arquivo = '$caminho_arquivo',
                                              nome_foto = '$nomefoto',
                                              caminho_foto = '$caminho_foto'    
                                        where id = $id";
        }else{
            $cnpj = $_POST['cnpj'];
            $razao_social = $_POST['razao_social'];
            $inscricao_estadual = $_POST['inscricao_estadual'];
            $nome_representante = isset($_POST['nome_representante']);
            
            $update_sql = "update vendedor set cnpj = '$cnpj',
                                              inscricao_estadual = '$inscricao_estadual', 
                                              razao_social = '$razao_social',
                                              nome_representante = '$nome_representante',  
                                              conta_deposito = '$conta_deposito',
                                              agencia_deposito = '$agencia_deposito',    
                                              observacao = '$observacao',
                                              endereco = '$endereco',
                                              bairro = '$bairro',
                                              numero = $numero,
                                              idcidade = $cidade,
                                              idestado = $idestado,
                                              idcategoria = $idcategoria,
                                              cep = '$cep',
                                              complemento = '$complemento',
                                              telefone = '$telefone',
                                              celular = '$celular',
                                              contato = '$contato',
                                              email = '$email',
                                              skype = '$skype',
                                              nome_arquivo = '$nomearquivo',
                                              caminho_arquivo = '$caminho_arquivo',
                                              nome_foto = '$nomefoto',
                                              caminho_foto = '$caminho_foto'
                                        where id = $id";
        }

        unset($dados['id']);
        unset($dados['nome']);
        unset($dados['tipopessoa']);
        unset($dados['conta_deposito']);
        unset($dados['agencia_deposito']);
        unset($dados['datanascimento']);
        unset($dados['sexo']);
        unset($dados['rg']);
        unset($dados['cpf']);
        unset($dados['observacao']);
        unset($dados['endereco']);
        unset($dados['bairro']);
        unset($dados['numero']);
        unset($dados['idcidade']);
        unset($dados['idestado']);
        unset($dados['idcategoria']);
        unset($dados['cep']);
        unset($dados['complemento']);
        unset($dados['telefone']);
        unset($dados['celular']);
        unset($dados['email']);
        unset($dados['skype']);
        unset($dados['arquivo']);
        unset($dados['caminho_arquivo']);
        unset($dados['cnpj']);
        unset($dados['razao_social']);
        unset($dados['inscricao_estadual']);
        unset($dados['contato']);
        unset($dados['nome_representante']);
        unset($dados['tipo_pessoa']);
        
        $query = $this->bd->prepare($update_sql);
        return $query->execute($dados);
    }

    public function excluir($id) {
        $diretorio = $_SERVER['DOCUMENT_ROOT'] . '/arca/protected/arquivos/uploads/';
        
        $consultaarquivo = "select caminho_arquivo, nome_arquivo from vendedor where id = " . $id . " and nome_arquivo <> ''";
        $sqlconsultaarquivo = $this->bd->prepare($consultaarquivo);
        $sqlconsultaarquivo->execute();
        //Exclusão de foto
        if ($sqlconsultaarquivo->rowCount() > 0) {
            foreach ($sqlconsultaarquivo as $rs) {
                $nomearquivo = $rs["nome_arquivo"];
                
                //Pega o caminho da imagem
                $apagararquivo = $diretorio . $nomearquivo;
                //Remover arquivo de imagem
                if (!unlink($apagararquivo)) {
                    echo ("<div style='color:red'><b>Não foi possível deletar o arquivo! </b></div>");
                }
            }
        }
        
        #FOTO
        $diretorio = $_SERVER['DOCUMENT_ROOT'] . '/arca/protected/arquivos/imagens/';
        
        $consultafoto = "select caminho_foto, nome_foto from vendedor where id = " . $id . " and nome_foto <> ''";
        $sqlconsultafoto = $this->bd->prepare($consultafoto);
        $sqlconsultafoto->execute();
        //Exclusão da foto
        if ($sqlconsultafoto->rowCount() > 0) {
            foreach ($sqlconsultafoto as $rs) {
                $nomefoto = $rs["nome_foto"];
                
                //Pega o caminho da imagem
                $apagarfoto = $diretorio . $nomefoto;
                //Remover arquivo de imagem
                if (!unlink($apagarfoto)) {
                    echo ("<div style='color:red'><b>Não foi possível deletar o arquivo! </b></div>");
                }
            }
        }
        
        //Exclui os dados do vendedor
        $sqlregistropessoa = "delete from vendedor where id = $id";
        $query = $this->bd->prepare($sqlregistropessoa);

        return $query->execute();
    }

}

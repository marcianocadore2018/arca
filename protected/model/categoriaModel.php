<?php

class CategoriaModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $descricao = $_POST['descricao'];
        $tipopessoa = $_POST['tipopessoa'];
        
        //Verifica Descrição
        $sql = "SELECT descricao FROM categoria WHERE descricao = '" . $descricao . "' AND tipopessoa = '". $tipopessoa ."'";
        $sql = $this->bd->prepare($sql);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            return "Descrição da categoria já existente. Favor informar outra descrição!";
        }else{
            $sql = "INSERT INTO categoria(descricao, tipopessoa) VALUES('$descricao', '$tipopessoa')";
        }
        unset($dados['id']);
        unset($dados['descricao']);
        unset($dados['tipopessoa']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }
    

    public function buscarTodos() {
        $sql = "select id, descricao, tipopessoa from categoria order by descricao asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
    
    public function buscarCategoriaTipoPessoa($tipopessoa) {
        $sql = "select id, descricao, tipopessoa from categoria where tipopessoa = '" . $tipopessoa . "' order by descricao asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
            $sql = "SELECT id, descricao, tipopessoa FROM categoria WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $id = $_POST['id'];
        $descricao = $_POST['descricao'];
        $tipopessoa = $_POST['tipopessoa'];
        
        $update_sql = "update categoria set descricao = '$descricao', tipopessoa = '$tipopessoa' WHERE id = $id";
        
        unset($dados['id']);
        unset($dados['descricao']);
        unset($dados['tipopessoa']);
        $query = $this->bd->prepare($update_sql);
        return $query->execute($dados);
    }

    public function excluir($id) {
        $sqlcategoria = "delete from categoria where id = $id";
        $query = $this->bd->prepare($sqlcategoria);
        return $query->execute();
    }

}

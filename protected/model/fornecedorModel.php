﻿<?php

class FornecedorModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        //Pegar o diretorio do projeto para o arquivo
        $diretorio = $_SERVER['DOCUMENT_ROOT'] . '/arca/protected/arquivos/uploads/';
        $caminhodiretorioarquivo = 'arca/protected/arquivos/uploads/';
        
        //Pegar o diretorio do projeto para a imagem
        $diretoriofoto = $_SERVER['DOCUMENT_ROOT'] . '/arca/protected/arquivos/imagens/';
        $caminhodiretoriofoto = 'arca/protected/arquivos/imagens/';
        $arquivo = $_FILES['arquivo']['name'];
        $nomearquivoaleatorio = $arquivo;
        
        $foto = $_FILES['foto']['name'];
        $nomefotoaleatorio = $foto;

        //se houver algum ponto ou virgula remover por nada
        //str_shuffle gera um nome aleatório
        $formatadonome = preg_replace('/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $nomearquivoaleatorio));
        $formatafotonome = preg_replace('/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $nomefotoaleatorio));
        
        $nomealeatorio = str_shuffle($formatadonome);
        $nomealeatoriofoto = str_shuffle($formatafotonome);
        
        $nomearquivo = str_replace(".", "", $nomealeatorio); // Primeiro tira os pontos
        $arquivo_tmp = $_FILES['arquivo']['tmp_name'];
        
        $nomefoto = str_replace(".", "", $nomealeatoriofoto); // Primeiro tira os pontos
        $foto_tmp = $_FILES['foto']['tmp_name'];
        
        //Pega extensão do arquivo
        $extensaoarquivo = substr($arquivo, -4);
        $destino = $diretorio . $nomearquivo . $extensaoarquivo;
        
        //Pega extensão da foto
        $extensaofoto = substr($foto, -4);
        $destinofoto = $diretoriofoto . $nomefoto . $extensaofoto;
        
        //alterar extensão de imagem para minúscula
        $res = strtolower($extensaoarquivo);
        $resfoto = strtolower($extensaofoto);
        
        #
        move_uploaded_file($arquivo_tmp, $destino);
        #Enviando a foto
        move_uploaded_file($foto_tmp, $destinofoto);
          
        //Gravar as informa~ções do diretório do arquivo
        $caminhoarquivo = $diretorio;
        //Gravar as informa~ções do nome do arquivo
        $nomearquivoaleatorio = $nomealeatorio;
          
        //Gravar as informa~ções do diretório da foto
        $caminhofoto = $diretoriofoto;
          
        //Gravar as informa~ções do nome da foto
        $nomefotoaleatoriofoto = $nomealeatoriofoto;
          
          
        #ARQUIVO
        $nomearquivoaleatoriosemponto = str_replace(".", "", $nomearquivoaleatorio);
        $nomearquivo = $nomearquivoaleatoriosemponto . $res;
          
        #FOTO
        $nomefotoaleatoriosemponto = str_replace(".", "", $nomefotoaleatoriofoto);
        $nomefoto = $nomefotoaleatoriosemponto . $resfoto;
          
        #}

        $tipopessoa = $_POST['tipopessoa'];
        $observacao = $_POST['observacao'];
        $situacaofornecedor = isset($_POST['situacaofornecedor']);
        $endereco = $_POST['endereco'];
        $bairro = $_POST['bairro'];
        $numero = $_POST['numero'];
        $cidade = $_POST['idcidade'];
        $idestado = $_POST['idestado'];
        $idcategoria = $_POST['idcategoria'];
        $cep = $_POST['cep'];
        $complemento = $_POST['complemento'];
        $telefone = $_POST['telefone'];
        $celular = $_POST['celular'];
        $contato = $_POST['contato'];
        $email = $_POST['email'];
        $skype = $_POST['skype'];
        $razao_social = $_POST['razao_social'];
        
        if ($_FILES['arquivo']['name'] != ""){
           $nomearquivo == $nomearquivo;
           $caminhoarquivo = $caminhoarquivo;
        }else{
           $nomearquivo = "";
           $caminhoarquivo = "";
        }
        
        if ($_FILES['foto']['name'] != ""){
           $nomefoto == $nomefoto;
           $caminhofoto = $caminhofoto;
        }else{
           $nomefoto = "";
           $caminhofoto = "";
        }

        if ($tipopessoa == 'PF') {
            $nome = $_POST['nome'];
            $datanascimento = $_POST['datanascimento'];
            $sexo = $_POST['sexo'];
            $rg = $_POST['rg'];
            $cpf = $_POST['cpf'];
            //Verifica CPF
            $sql = "SELECT cpf FROM fornecedor WHERE cpf = '" . $cpf . "'";

            $sql = $this->bd->prepare($sql);
            $sql->execute();

            if ($sql->rowCount() > 0) {
                return "CPF do fornecedor já existente. Favor informar outro CPF!";
            } else {
                $sql = "INSERT INTO fornecedor(nome, idcategoria, tipo_pessoa, datanascimento, genero, rg, cpf, observacao, endereco, bairro, numero, idcidade, idestado, cep, complemento, telefone, celular, contato, email, skype, nome_arquivo, caminho_arquivo, nome_foto, caminho_foto) "
                        . " VALUES('$nome', $idcategoria, '$tipopessoa', '$datanascimento', '$sexo', '$rg', '$cpf', '$observacao', '$endereco', '$bairro', $numero, $cidade, $idestado, '$cep', '$complemento', '$telefone', '$celular', '$contato', '$email', '$skype', '$nomearquivo', '$caminhodiretorioarquivo', '$nomefoto', '$caminhodiretoriofoto')";
            }
        } else {
            $cnpj = $_POST['cnpj'];
            $inscricao_estadual = $_POST['inscricao_estadual'];
            $nome_representante = isset($_POST['nome_representante']);

            //Verifica CNPJ
            $sql = "SELECT cnpj FROM fornecedor WHERE cpf = '" . $cnpj . "'";
            $sql = $this->bd->prepare($sql);
            $sql->execute();

            if ($sql->rowCount() > 0) {
                return "CNPJ do fornecedor já existente. Favor informar outro CNPJ!";
            } else {
                $sql = "INSERT INTO fornecedor(tipo_pessoa, idcategoria, razao_social, cnpj, inscricao_estadual, nome_representante, observacao, endereco, bairro, numero, idcidade, idestado, cep, complemento, telefone, celular, contato, email, skype, nome_arquivo, caminho_arquivo, nome_foto, caminho_foto) "
                        . " VALUES('$tipopessoa', $idcategoria, '$razao_social','$cnpj', '$inscricao_estadual', '$nome_representante', '$observacao', '$endereco', '$bairro', $numero, $cidade, $idestado, '$cep', '$complemento', '$telefone', '$celular', '$contato', '$email', '$skype', '$nomearquivo', '$caminhodiretorioarquivo', '$nomefoto', '$caminhodiretoriofoto')";
            }
        }

        unset($dados['id']);
        unset($dados['nome']);
        unset($dados['tipopessoa']);
        unset($dados['datanascimento']);
        unset($dados['razao_social']);
        unset($dados['sexo']);
        unset($dados['rg']);
        unset($dados['cpf']);
        unset($dados['observacao']);
        unset($dados['endereco']);
        unset($dados['bairro']);
        unset($dados['numero']);
        unset($dados['idcidade']);
        unset($dados['idestado']);
        unset($dados['idcategoria']);
        unset($dados['cep']);
        unset($dados['complemento']);
        unset($dados['telefone']);
        unset($dados['celular']);
        unset($dados['email']);
        unset($dados['skype']);
        unset($dados['arquivo']);
        unset($dados['caminho_arquivo']);
        unset($dados['foto']);
        unset($dados['caminho_foto']);
        unset($dados['cnpj']);
        unset($dados['inscricao_estadual']);
        unset($dados['contato']);
        unset($dados['nome_representante']);
        unset($dados['tipo_pessoa']);

        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        $sql = "select forn.id as id,
                       forn.tipo_pessoa as tipopessoa,
                       forn.nome as nomefornecedor, 
                       to_char(forn.datanascimento, 'dd/MM/yyyy') as datanascimento, 
                       forn.genero as genero, 
                       forn.rg as rg, 
                       forn.cpf as cpf,
                       forn.razao_social as razao_social,
                       forn.cnpj as cnpj,
                       forn.inscricao_estadual as inscricaoestadual,
                       (initcap(cid.nome) || ' - ' || forn.endereco || ' - ' || forn.bairro || ' - ' || est.uf) as endereco_fornecedor,
                       forn.telefone as telefone, 
                       forn.celular as celular, 
                       forn.email as email, 
                       forn.skype as skype,
                       cate.descricao as descricaocategoria,
                       (forn.caminho_arquivo || forn.nome_arquivo) as arquivo,
                       ('/' || forn.caminho_arquivo) as caminho_arquivo,
                       forn.nome_arquivo as nome_arquivo,
                       ('/' || forn.caminho_foto || forn.nome_foto) as foto,
                       ('/' || forn.caminho_foto) as caminho_foto,
                       forn.nome_foto as nome_foto
                  from fornecedor forn
                 inner join estado est 
                    on forn.idestado = est.id
                 inner join categoria cate
                    on forn.idcategoria = cate.id
                 inner join cidade cid
                    on forn.idcidade = cid.id
                 order by forn.nome asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
        if (isset($_SERVER['HTTPS'])) {
            $prefixo = 'https://';
        } else {
            $prefixo = 'http://';
        }

        //URL BASE
        $urlbase = $prefixo . '' . $_SERVER['HTTP_HOST'] . '/';

        $sql = "SELECT forn.id as id,
                       forn.nome as nome,
                       to_char(forn.datanascimento, 'dd/MM/yyyy') as datanascimento,
                       forn.rg as rg,
                       forn.razao_social as razao_social,
                       forn.cpf as cpf,
                       forn.cnpj as cnpj,
                       forn.tipo_pessoa as tipo_pessoa,
                       forn.inscricao_estadual as inscricao_estadual,
                       forn.nome_representante as nome_representante,
                       forn.genero as sexo,
                       forn.observacao as observacao,
                       forn.endereco as endereco,
                       forn.numero as numero, 
                       forn.bairro as bairro,
                       forn.idcidade as idcidade,
                       forn.cep as cep,
                       forn.complemento as complemento,
                       forn.telefone as telefone,
                       forn.celular as celular,
                       forn.contato as contato,
                       forn.email as email,
                       forn.skype as skype,
                       forn.idestado as idestado,
                       cate.id as idcategoria,
                       cate.descricao as descricaocategoria,
                       (forn.caminho_arquivo || forn.nome_arquivo) as arquivo,
                       forn.nome_arquivo as nome_arquivo,
                       (forn.caminho_foto || forn.nome_foto) as foto,
                       forn.nome_foto as nome_foto
                  FROM fornecedor forn
                 INNER JOIN estado est 
                    on forn.idestado = est.id
                 INNER JOIN categoria cate
                    on forn.idcategoria = cate.id
                 INNER JOIN cidade cid 
                    on forn.idcidade = cid.id
                 WHERE forn.id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $diretorio = $_SERVER['DOCUMENT_ROOT'] . '/arca/protected/arquivos/uploads/';
        $caminhodiretorio = 'arca/protected/arquivos/uploads/';

        #FOTO
        $diretoriofoto = $_SERVER['DOCUMENT_ROOT'] . '/arca/protected/arquivos/imagens/';
        $caminhodiretoriofoto = 'arca/protected/arquivos/imagens/';
        
        //Consulta se nome da imagem ja existe e exclui a imagem do diretório se o usuário alterar um registro 
        //que ja possui o mesmo nome da imagem
        $consultaarquivo = "select caminho_arquivo, nome_arquivo from fornecedor where id = " . $_POST['id'];
        $sqlconsultaarquivo = $this->bd->prepare($consultaarquivo);
        $sqlconsultaarquivo->execute();


        if ($_FILES['arquivo']['name'] != '') {
            $arquivo = $_FILES['arquivo']['name'];
            $arquivoaleatorio = $arquivo;
            $formatacao = preg_replace('/[`^~\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $arquivoaleatorio));
            $nomealeatorio = str_shuffle($formatacao);

            //se houver algum ponto ou virgula remover por nada
            $nomearquivo = str_replace(".", "", $nomealeatorio);
            $arquivo_tmp = $_FILES['arquivo']['tmp_name'];

            $extensaoarquivo = substr($arquivo, -4);

            //alterar extensão de imagem para minúscula
            $destino = $diretorio . $nomearquivo . $extensaoarquivo;
            //Converter a extensão para letra minúscula
            $res = strtolower($extensaoarquivo);

            //Verificar se o arquivo esta indo vazio
            if ($res != '') {
                $arquivo_tmp = $_FILES['arquivo']['tmp_name'];
                move_uploaded_file($arquivo_tmp, $destino);
                //Gravar essas informações no banco, vai ter o caminho e o nome da foto.
                $caminho_arquivo = $caminhodiretorio;
                $nomearquivoaleatorio = $nomealeatorio;
                $nomearquivoaleatoriosemponto = str_replace(".", "", $nomearquivoaleatorio);
                $nomearquivo = $nomearquivoaleatoriosemponto . $extensaoarquivo;
            }
        } else {
            if ($sqlconsultaarquivo->rowCount() > 0) {
                foreach ($sqlconsultaarquivo as $rs) {
                    $nomearquivo = $rs["nome_arquivo"];
                    $caminho_arquivo = $rs["caminho_arquivo"];
                }
            }
        }
        
        //Consulta se nome da imagem ja existe e exclui a imagem do diretório se o usuário alterar um registro 
        //que ja possui o mesmo nome da imagem
        $consultafoto = "select caminho_foto, nome_foto from fornecedor where id = " . $_POST['id'];
        $sqlconsultafoto = $this->bd->prepare($consultafoto);
        $sqlconsultafoto->execute();
        
        if ($_FILES['foto']['name'] != '') {
            #FOTO
            $foto = $_FILES['foto']['name'];
            $fotoaleatorio = $foto;
            $formatacaofoto = preg_replace('/[`^~\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $fotoaleatorio));
            $nomefotoaleatorio = str_shuffle($formatacaofoto);

            //FOTO se houver algum ponto ou virgula remover por nada
            $nomefoto = str_replace(".", "", $nomefotoaleatorio);
            $foto_tmp = $_FILES['foto']['tmp_name'];
            $extensaofoto = substr($foto, -4);

            //FOTO
            //alterar extensão de imagem para minúscula
            $destinofoto = $diretoriofoto . $nomefoto . $extensaofoto;
            //Converter a extensão para letra minúscula
            $resfoto = strtolower($extensaofoto);

            //Verificar se o arquivo esta indo vazio
            if ($resfoto != '') {
                #FOTO
                $foto_tmp = $_FILES['foto']['tmp_name'];
                move_uploaded_file($foto_tmp, $destinofoto);
                $caminho_foto = $caminhodiretoriofoto;
                $nomefotoaleatorio = $nomefotoaleatorio;
                $nomefotoaleatoriosemponto = str_replace(".", "", $nomefotoaleatorio);
                $nomefoto = $nomefotoaleatoriosemponto . $extensaofoto;
            }
        } else {
            if ($sqlconsultafoto->rowCount() > 0) {
                foreach ($sqlconsultafoto as $rs) {
                    $nomefoto = $rs["nome_foto"];
                    $caminho_foto = $rs["caminho_foto"];
                }
            }
        }

        $id = $_POST['id'];
        $observacao = $_POST['observacao'];
        $endereco = $_POST['endereco'];
        $bairro = $_POST['bairro'];
        $numero = $_POST['numero'];
        $cidade = $_POST['idcidade'];
        $idestado = $_POST['idestado'];
        $idcategoria = $_POST['idcategoria'];
        $cep = $_POST['cep'];
        $complemento = $_POST['complemento'];
        $telefone = $_POST['telefone'];
        $celular = $_POST['celular'];
        $contato = $_POST['contato'];
        $email = $_POST['email'];
        $skype = $_POST['skype'];

        if ($_POST['tipopessoa'] == 'PF') {
            $nome = $_POST['nome'];
            $datanascimento = $_POST['datanascimento'];
            $sexo = $_POST['sexo'];
            $rg = $_POST['rg'];
            $cpf = $_POST['cpf'];

            $update_sql = "update fornecedor set nome = '$nome',
                                              datanascimento = '$datanascimento',
                                              genero = '$sexo',
                                              rg = '$rg',
                                              cpf = '$cpf', 
                                              observacao = '$observacao',
                                              endereco = '$endereco',
                                              bairro = '$bairro',
                                              numero = $numero,
                                              idcidade = $cidade,
                                              idestado = $idestado,
                                              idcategoria = $idcategoria,    
                                              cep = '$cep',
                                              complemento = '$complemento',
                                              telefone = '$telefone',
                                              celular = '$celular',
                                              contato = '$contato',
                                              email = '$email',
                                              skype = '$skype',
                                              nome_arquivo = '$nomearquivo',
                                              caminho_arquivo = '$caminho_arquivo',
                                              nome_foto = '$nomefoto',
                                              caminho_foto = '$caminho_foto'
                                        where id = $id";
        } else {
            $cnpj = $_POST['cnpj'];
            $razao_social = $_POST['razao_social'];
            $inscricao_estadual = $_POST['inscricao_estadual'];
            $nome_representante = isset($_POST['nome_representante']);

            $update_sql = "update fornecedor set cnpj = '$cnpj',
                                              inscricao_estadual = '$inscricao_estadual', 
                                              razao_social = '$razao_social',
                                              nome_representante = '$nome_representante',  
                                              observacao = '$observacao',
                                              endereco = '$endereco',
                                              bairro = '$bairro',
                                              numero = $numero,
                                              idcidade = $cidade,
                                              idestado = $idestado,
                                              idcategoria = $idcategoria,
                                              cep = '$cep',
                                              complemento = '$complemento',
                                              telefone = '$telefone',
                                              celular = '$celular',
                                              contato = '$contato',
                                              email = '$email',
                                              skype = '$skype',
                                              nome_arquivo = '$nomearquivo',
                                              caminho_arquivo = '$caminho_arquivo',
                                              nome_foto = '$nomefoto',
                                              caminho_foto = '$caminho_foto'    
                                        where id = $id";
        }

        unset($dados['id']);
        unset($dados['nome']);
        unset($dados['tipopessoa']);
        unset($dados['tipo_pessoa']);
        unset($dados['datanascimento']);
        unset($dados['sexo']);
        unset($dados['rg']);
        unset($dados['cpf']);
        unset($dados['observacao']);
        unset($dados['endereco']);
        unset($dados['bairro']);
        unset($dados['numero']);
        unset($dados['idcidade']);
        unset($dados['idestado']);
        unset($dados['idcategoria']);
        unset($dados['cep']);
        unset($dados['complemento']);
        unset($dados['telefone']);
        unset($dados['celular']);
        unset($dados['email']);
        unset($dados['skype']);
        unset($dados['arquivo']);
        unset($dados['caminho_arquivo']);
        unset($dados['foto']);
        unset($dados['caminho_foto']);
        unset($dados['cnpj']);
        unset($dados['razao_social']);
        unset($dados['inscricao_estadual']);
        unset($dados['contato']);
        unset($dados['nome_representante']);

        $query = $this->bd->prepare($update_sql);
        return $query->execute($dados);
    }

    public function excluir($id) {
        $diretorio = $_SERVER['DOCUMENT_ROOT'] . '/arca/protected/arquivos/uploads/';
        
        $consultaarquivo = "select caminho_arquivo, nome_arquivo from fornecedor where id = " . $id . " and nome_arquivo <> ''";
        $sqlconsultaarquivo = $this->bd->prepare($consultaarquivo);
        $sqlconsultaarquivo->execute();
        //Exclusão de foto
        if ($sqlconsultaarquivo->rowCount() > 0) {
            foreach ($sqlconsultaarquivo as $rs) {
                $nomearquivo = $rs["nome_arquivo"];
                
                //Pega o caminho da imagem
                $apagararquivo = $diretorio . $nomearquivo;
                //Remover arquivo de imagem
                if (!unlink($apagararquivo)) {
                    echo ("<div style='color:red'><b>Não foi possível deletar o arquivo! </b></div>");
                }
            }
        }
        
        #FOTO
        $diretorio = $_SERVER['DOCUMENT_ROOT'] . '/arca/protected/arquivos/imagens/';
        
        $consultafoto = "select caminho_foto, nome_foto from fornecedor where id = " . $id . " and nome_foto <> ''";
        $sqlconsultafoto = $this->bd->prepare($consultafoto);
        $sqlconsultafoto->execute();
        //Exclusão da foto
        if ($sqlconsultafoto->rowCount() > 0) {
            foreach ($sqlconsultafoto as $rs) {
                $nomefoto = $rs["nome_foto"];
                
                //Pega o caminho da imagem
                $apagarfoto = $diretorio . $nomefoto;
                //Remover arquivo de imagem
                if (!unlink($apagarfoto)) {
                    echo ("<div style='color:red'><b>Não foi possível deletar o arquivo! </b></div>");
                }
            }
        }
        
        //Exclui os dados do funcionário
        $sqlregistropessoa = "delete from fornecedor where id = $id";
        $query = $this->bd->prepare($sqlregistropessoa);

        return $query->execute();
    }

}

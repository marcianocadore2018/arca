<?php

class UsuarioModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        date_default_timezone_set('America/Sao_Paulo');
        $datacadastro = date('d/m/Y');
        
        $sql = "INSERT INTO usuario(login, tipousuario, nomeusuario, senhausuario, datacadastro) VALUES(:login, :tipousuario, :nomeusuario, :senhausuario, '$datacadastro')";
        unset($dados['id']);
        unset($dados['datacadastro']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }
    

    public function buscarTodos() {
        $sql = "select id, login, tipousuario, nomeusuario, senhausuario, to_char(datacadastro, 'dd/MM/yyyy') as datacadastro from usuario order by datacadastro asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($id) {
            $sql = "SELECT id, login, tipousuario, nomeusuario, senhausuario, to_char(datacadastro, 'dd/MM/yyyy') as datacadastro FROM usuario WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $id));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $id = $_POST['id'];
        
        $update_sql = "update usuario 
                          set login = :login, 
                          nomeusuario = :nomeusuario,
                          senhausuario = :senhausuario,
                          tipousuario = :tipousuario
                    WHERE id = $id";
        
        unset($dados['id']);
        $query = $this->bd->prepare($update_sql);
        return $query->execute($dados);
    }

    public function excluir($id) {
        $sqlcategoria = "delete from usuario where id = $id";
        $query = $this->bd->prepare($sqlcategoria);
        return $query->execute();
    }

}

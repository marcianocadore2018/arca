<?php

class FreteController {
    private $bd, $model;
    
    function __construct() {
        $this->model = new FreteModel();
    }
    
    public function novo() {
        ///Ação padrão para categoria
        $acao       = 'index.php?controle=freteController&acao=inserir';
        require './protected/view/frete/formFrete.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        
        if($r){
            echo '<div class="alert alert-success">
                    Dados gravados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao cadastrar os dados.
                  </div>';
        }
        
        $this->listar();
    }
    
    public function listar() {
        $listaDados   = $this->model->buscarTodos();
        require './protected/view/frete/listFrete.php';
    }
    
    public function buscar($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);
        
        $frete      = $this->model->buscar($id);
        $acao         = 'index.php?controle=freteController&acao=atualizar';
        require './protected/view/frete/formFrete.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao atualizar os dados.
                  </div>';
        }
        
        $this->listar();
    }
    
    public function excluir($id){
        $decodeget = base64_decode($id);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);
        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir o Frete pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}
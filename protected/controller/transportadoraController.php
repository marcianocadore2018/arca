<?php

class TransportadoraController {
    private $bd, $model, $modelEstados;
    
    function __construct() {
        $this->model        = new TransportadoraModel();
        $this->modelEstados = new EstadoModel();
        $this->modelCidades = new CidadeModel();
        $this->modelCategorias = new CategoriaModel();
    }
    
    public function novo() {
        $listaEstados = $this->modelEstados->buscarTodos();
        $listaCidades = $this->modelCidades->buscarTodos();
        $listaCategorias = $this->modelCategorias->buscarCategoriaTipoPessoa($tipopessoa='TP');
        ///Ação padrão para transportadora
        $acao       = 'index.php?controle=transportadoraController&acao=inserir';
        require './protected/view/transportadora/formTransportadora.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados gravados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao cadastrar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados   = $this->model->buscarTodos();
        require './protected/view/transportadora/listTransportadora.php';
    }
    
    public function buscar($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);
        
        $listaEstados = $this->modelEstados->buscarTodos();
        $listaCidades = $this->modelCidades->buscarTodos();
        $listaCategorias = $this->modelCategorias->buscarCategoriaTipoPessoa($tipopessoa='TP');
        $transportadora  = $this->model->buscar($id);
        $acao         = 'index.php?controle=transportadoraController&acao=atualizar';
        require './protected/view/transportadora/formTransportadora.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r){
            echo '<div class="alert alert-success">
                    Dados atualizados com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Erro ao atualizar os dados.
                  </div>';
        }
        $this->listar();
    }
    
    public function excluir($id){
        $decodeget = base64_decode($id);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);
        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir o Transportadora pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}
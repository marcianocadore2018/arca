<?php

class TsiController {
    private $bd, $model;
    
    function __construct() {
        $this->model = new TsiModel();
    }
    
    public function novo() {
        ///Ação padrão para categoria
        $acao       = 'index.php?controle=tsiController&acao=inserir';
        require './protected/view/tsi/formTsi.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        
        if($r[0] == '00000'){
             echo '<div class="alert alert-danger">
                        Não foi possível adicionar esse TSI pois o nome já encontra-se cadastrado!
                      </div>';
        }else{
            if($r){
                echo '<div class="alert alert-success">
                        Dados gravados com <strong>Sucesso</strong>.
                      </div>';
            }else{
                echo '<div class="alert alert-danger">
                        Erro ao cadastrar os dados.
                      </div>';
            }
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados   = $this->model->buscarTodos();
        require './protected/view/tsi/listTsi.php';
    }
    
    public function buscar($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);
        
        $tsi      = $this->model->buscar($id);
        $acao         = 'index.php?controle=tsiController&acao=atualizar';
        require './protected/view/tsi/formTsi.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r[0] == '00000'){
             echo '<div class="alert alert-danger">
                        Não foi possível atualizar esse TSI pois o nome já encontra-se cadastrado!
                      </div>';
        }else{
            if($r){
                echo '<div class="alert alert-success">
                        Dados atualizados com <strong>Sucesso</strong>.
                      </div>';
            }else{
                echo '<div class="alert alert-danger">
                        Erro ao atualizar os dados.
                      </div>';
            }
        }
        $this->listar();
    }
    
    public function excluir($id){
        $decodeget = base64_decode($id);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);
        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir o TSI pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}
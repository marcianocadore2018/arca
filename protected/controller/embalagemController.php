<?php

class EmbalagemController {
    private $bd, $model;
    
    function __construct() {
        $this->model = new EmbalagemModel();
    }
    
    public function novo() {
        ///Ação padrão para categoria
        $acao       = 'index.php?controle=embalagemController&acao=inserir';
        require './protected/view/embalagem/formEmbalagem.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r[0] == '00000'){
             echo '<div class="alert alert-danger">
                        Não foi possível adicionar essa Embalagem pois a descrição já encontra-se cadastrada!
                      </div>';
        }else{
            if($r){
                echo '<div class="alert alert-success">
                        Dados gravados com <strong>Sucesso</strong>.
                      </div>';
            }else{
                echo '<div class="alert alert-danger">
                        Erro ao cadastrar os dados.
                      </div>';
            }
        }
        $this->listar();
    }
    
    public function listar() {
        $listaDados   = $this->model->buscarTodos();
        require './protected/view/embalagem/listEmbalagem.php';
    }
    
    public function buscar($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);
        
        $embalagem      = $this->model->buscar($id);
        $acao         = 'index.php?controle=embalagemController&acao=atualizar';
        require './protected/view/embalagem/formEmbalagem.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r[0] == '00000'){
             echo '<div class="alert alert-danger">
                        Não foi possível atualizar essa Embalagem pois a descrição já encontra-se cadastrada!
                      </div>';
        }else{
            if($r){
                echo '<div class="alert alert-success">
                        Dados atualizados com <strong>Sucesso</strong>.
                      </div>';
            }else{
                echo '<div class="alert alert-danger">
                        Erro ao atualizar os dados.
                      </div>';
            }
        }
        $this->listar();
    }
    
    public function excluir($id){
        $decodeget = base64_decode($id);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);
        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir a Embalagem pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}
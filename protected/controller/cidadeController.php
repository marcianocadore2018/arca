<?php

class CidadeController {
    private $bd, $model;
    
    function __construct() {
        $this->model = new CidadeModel();
        $this->modelEstados = new EstadoModel();
    }
    
    public function novo() {
        ///Ação padrão para categoria
        $listaEstados = $this->modelEstados->buscarTodos();
        $acao       = 'index.php?controle=cidadeController&acao=inserir';
        require './protected/view/cidade/formCidade.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r[0] == '00000'){
             echo '<div class="alert alert-danger">
                        Não foi possível adicionar a Cidade pois o nome já encontra-se cadastrado!
                      </div>';
        }else{
            if($r){
                echo '<div class="alert alert-success">
                        Dados gravados com <strong>Sucesso</strong>.
                      </div>';
            }else{
                echo '<div class="alert alert-danger">
                        Não foi possível cadastrar a cidade pois a mesma já encontra-se cadastrada.
                      </div>';
            }
            $this->listar();
        }
    }
    
    public function listar() {
        $listaDados   = $this->model->buscarTodos();
        require './protected/view/cidade/listCidade.php';
    }
    
    public function buscar($id) {
        $idget = $_GET['id'];
        $decodeget = base64_decode($idget);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);
        
        $listaEstados = $this->modelEstados->buscarTodos();
        $cidade      = $this->model->buscar($id);
        $acao         = 'index.php?controle=cidadeController&acao=atualizar';
        require './protected/view/cidade/formCidade.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r[0] == '00000'){
             echo '<div class="alert alert-danger">
                        Não foi possível atualizar a Cidade pois o nome já encontra-se cadastrado!
                      </div>';
        }else{
            if($r){
                echo '<div class="alert alert-success">
                        Dados atualizados com <strong>Sucesso</strong>.
                      </div>';
            }else{
                echo '<div class="alert alert-danger">
                        Erro ao atualizar os dados.
                      </div>';
            }
        }
        $this->listar();
    }
    
    public function excluir($id){
        $decodeget = base64_decode($id);
        $remover = str_replace("passar", "", $decodeget);
        $id = str_replace("metodoget", "", $remover);
        $r = $this->model->excluir($id);
        if($r){
            echo '<div class="alert alert-success">
                    Dados Removidos com <strong>Sucesso</strong>.
                  </div>';
        }else{
            echo '<div class="alert alert-danger">
                    Não foi possível excluir a Cidade pois possui registros filhos.
                  </div>';
        }
        $this->listar();
    }
}
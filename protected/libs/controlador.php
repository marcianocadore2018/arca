<?php

Class Controlador {

    function __construct() {

        if (isset($_GET['controle'])) {
            $ctrlNome = $_GET['controle'];

            $arquivo = './protected/controller/' . $ctrlNome . '.php';

            if (file_exists($arquivo)) {
                ?>
                <?php
                require $arquivo;

                $controle = new $ctrlNome();

                if ($_GET['acao'] == "novo" || $_GET['acao'] == "listar") {
                    $controle->{$_GET['acao']}();
                } else if ($_GET['acao'] == "inserir" || $_GET['acao'] == "atualizar" || $_GET['acao'] == "filtrarpagamento" || $_GET['acao'] == "filtrartrocaproduto" || $_GET['acao'] == "trocarproduto") {
                    $controle->{$_GET['acao']}($_POST);
                } else if ($_GET['acao'] == "buscar" || $_GET['acao'] == "excluir") {
                    $controle->{$_GET['acao']}($_GET['id']);
                } else if ($_GET['acao'] == "buscarToCliente") {
                    $controle->{$_GET['acao']}($_GET['id']);
                } else if ($_GET['acao'] == "inserirRenda") {
                    $controle->{$_GET['acao']}($_POST);
                } else if ($_GET['acao'] == "buscarToIAColaborador") {
                    $controle->{$_GET['acao']}($_GET['id']);
                } else if ($_GET['acao'] == "inserirIAColaborador") {
                    $controle->{$_GET['acao']}($_POST);
                } else if ($_GET['acao'] == "filtroEstoque") {
                    $controle->{$_GET['acao']}($_POST);
                }
            }
        } else {
            ?>
            <div id="conteudo">
                <style>

                </style>
                <center>
                    <table style="margin-top: 15%">
                        <tr>
                        </tr>
                    </table>
                </center>
                <br><br><br><br>
                <center>
                    <table>
                        <tr>
                            <th>
                                <h4 id="qtd_email">
                            </th>
                        </tr>
                    </table>
                </center>
            </div>
            <footer class="footer" style="position: fixed; bottom:0; left:0;">
                <div style="margin-top: 5px; margin-bottom: 5px; text-align: center; color: white">Sistema Arca - 2020 <br/>
                    Sistema Arca</div>
            </footer>
        <?php
        }
    }

}
?>
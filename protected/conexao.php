<?php

require_once './config/config.php';

class Conexao {
    public $bd;
    
    function __construct() {
        $this->estabeleceConexao();
    }
    
    public function estabeleceConexao(){
        try{
            $this->bd = new PDO(BD_TIPO .':host='.BD_HOST
                    .';dbname='.BD_NOME, BD_USER, BD_SENHA);
        }  catch (PDOException $e){
            echo "Erro na conexao com o banco de dados!".$e;
        }
    }
}

//Pegar data e hora para a aplicação
date_default_timezone_set('America/Sao_Paulo');
$data = date('d/m/Y');
$hora = date('H:i');

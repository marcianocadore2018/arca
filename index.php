<?php
#include ( 'protected/libs/Email.php' );                             # Include da lib e-mail sent
require_once ('./protected/conexao.php');

# LOAD DINAMIC MODELS
foreach (glob("./protected/model/*.php") as $filename) {
    include $filename;
}

require_once ( './protected/libs/controlador.php' );                # Include Default controller
require_once ( './controlelogin/conectadosession.php');             # Include session controller
require_once ( './protected/libs/verificadiretorio.php' );

#include ( './ws/index.php' );                                       # Include WS (WebService) start 
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>ARCA</title>
        <link href="includes/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="includes/css/datatables.min.css">
        <link rel="stylesheet" href="includes/css/bootstrap-select.min.css">
        <link rel="shortcut icon" href="" type="image/x-icon"/>
        <link rel="stylesheet" href="includes/css/redmond/jquery-ui-1.10.1.custom.css">
        <script src="includes/js/jquery-1.9.1.js" type="text/javascript"></script>
        <script src="includes/js/jquery-ui.js" type="text/javascript"></script>
        <script src="includes/js/jquery.maskMoney.js" type="text/javascript"></script>
        <script src="includes/js/jquery.maskedinput.min.js" type="text/javascript"></script>
        <script src="includes/js/validarcampos.js" type="text/javascript"></script>
        <script src="includes/js/vanilla-masker.min.js" type="text/javascript"></script>
        <script src="includes/js/bootstrap-select.min.js" type="text/javascript"></script>
        
        <!-- CSS Customizado-->
        <link rel="stylesheet" href="includes/css/customizado_aplicacao.css">

        <!-- Excluir Registro - Mensagem-->
        <script src="includes/js/jquery-1.11.3.js"></script>
        <script src="includes/js/jquery-ui.js"></script>
        <link rel="stylesheet" href="includes/css/jquery-ui.css" type="text/css" />

        <!-- Chosen -->
        <script src="includes/js/plugins/chosen/chosen.jquery.js"></script>
        <!-- Chosen -->
        <link href="includes/css/plugins/chosen/chosen.css" rel="stylesheet">
        
        <script src="includes/js/modal_exclusao.js"></script>
        
        <script src="includes/js/jquery.easy-confirm-dialog.js"></script>
    </head>
</head>
<body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top" style="background-color: #006621">
        <div class="container" style="padding-left: 0px">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#" style=""  id="menutitle">Arca</a>


            </div>
            <p>
            <lable style="color: white;">
                <b style="font-size: 12px;">Logado como:</b> <?php echo $_SESSION['login'];  ?>&nbsp;&nbsp;&nbsp;
                <b style="font-size: 12px;">Data/Hora:</b>&nbsp; <?php echo $data . ' - ' . $hora;  ?>
            </lable>
            </p>

            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="index.php"  id="menutitle">Home</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"  id="menutitle" aria-expanded="false">Cadastros<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?controle=clienteController&acao=listar">Cadastro de Clientes</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="index.php?controle=fornecedorController&acao=listar">Cadastro de Fornecedores</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="index.php?controle=vendedorController&acao=listar">Cadastro de Vendedores</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="index.php?controle=transportadoraController&acao=listar">Cadastro de Transportadoras</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="index.php?controle=clienteautorizadoController&acao=listar">Cadastro & Pesquisa de Produtos & Estoque</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="index.php?controle=clienteautorizadoController&acao=listar">Reposição de Estoque</a></li>
                            <li role="separator" class="divider"></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"  id="menutitle" aria-expanded="false">Vendas<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?controle=colaboradoresController&acao=listar">Configurar parâmetros de Venda</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="index.php?controle=documentacaoController&acao=listar">Pesquisar Contas a Receber</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="index.php?controle=cargoController&acao=listar">Pesquisar Contas a Pagar</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="index.php?controle=setorController&acao=listar">Gerenciar Movimentos de Caixa</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="index.php?controle=informacaoadicionalcolaboradorController&acao=listar">Limite de Desconto</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="index.php?controle=informacaoadicionalcolaboradorController&acao=listar">Comissão por Produtos</a></li>
                            <li role="separator" class="divider"></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"  id="menutitle" aria-expanded="false">Utilitários<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?controle=categoriaController&acao=listar">Categoria</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="index.php?controle=usuarioController&acao=listar">Cadastro de Usuário</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="index.php?controle=cidadeController&acao=listar">Cadastro de Cidade</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="index.php?controle=tsiController&acao=listar">Cadastro de TSI</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="index.php?controle=embalagemController&acao=listar">Cadastro de Embalagem</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="index.php?controle=freteController&acao=listar">Cadastro de Frete</a></li>
                            <li role="separator" class="divider"></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"  id="menutitle" aria-haspopup="true" aria-expanded="false">Relatórios<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#" data-toggle="modal" data-target="#modalVendas">Produtos Mais Vendidos</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#" data-toggle="modal" data-target="#modalProdutos">Produtos Menos Vendidos</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#" data-toggle="modal" data-target="#modalPagamento">Clientes que Mais Compram</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#" data-toggle="modal" data-target="#modalPagamento">Clientes que Menos Compram</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#" data-toggle="modal" data-target="#modalPagamento">Vendedores que Mais Venderam</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#" data-toggle="modal" data-target="#modalPagamento">Vendedores que Menos Venderam</a></li>
                            <li role="separator" class="divider"></li>
                        </ul>
                    </li>
                    <li><a href="/arca/controlelogin/sair.php"  id="menutitle">Sair</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    
    <div class="col-md-12">
        <?php
        $app = new Controlador();
        ?>
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="includes/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="includes/js/jquery.dataTables.min.js"></script>
    <script src="includes/js/dataTables.bootstrap.min.js"></script>
    <script src="includes/js/datatable_sistema.js"></script>

    <div style="display: none">
        <div id="dialog" title="Exclusão do Registro" style="font-size: 14px;">
            <img src="includes/imagens/question.png" alt="" />
            Tem certeza que deseja excluir?
        </div>
</div>

</body>
</html>